"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QueryBuilder = (function () {
    function QueryBuilder(query, args) {
        this._query = '';
        this._query = query;
        this._queryArgs = args;
    }
    Object.defineProperty(QueryBuilder.prototype, "query", {
        get: function () {
            return this._query;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QueryBuilder.prototype, "queryArgs", {
        get: function () {
            return this._queryArgs;
        },
        enumerable: true,
        configurable: true
    });
    return QueryBuilder;
}());
exports.QueryBuilder = QueryBuilder;
//# sourceMappingURL=query-builder.js.map