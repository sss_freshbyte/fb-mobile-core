"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Serializable = (function () {
    function Serializable() {
    }
    Serializable.prototype.serialize = function () {
        return JSON.stringify(this.serializable);
    };
    Object.defineProperty(Serializable.prototype, "serializable", {
        get: function () {
            var obj = {};
            for (var prop in this) {
                if (this.hasOwnProperty(prop) && prop.startsWith('_') && prop !== '_db' && prop !== '_logger' && prop !== '_pricingService') {
                    if (this[prop] instanceof Array) {
                        var arrayPropTemp = [];
                        for (var arrayObj in this[prop]) {
                            if (this[prop][arrayObj] instanceof Serializable) {
                                var sObject = this[prop][arrayObj];
                                arrayPropTemp.push(sObject.serializable);
                            }
                        }
                        obj[prop.slice(1)] = arrayPropTemp;
                    }
                    else {
                        obj[prop.slice(1)] = this[prop];
                    }
                }
            }
            return obj;
        },
        enumerable: true,
        configurable: true
    });
    return Serializable;
}());
exports.Serializable = Serializable;
//# sourceMappingURL=serializable.model.js.map