export declare class Serializable {
    serialize(): string;
    readonly serializable: Object;
}
