import { ValidatorFn } from '@angular/forms';
export declare class CustomValidators {
    static min(min: number): ValidatorFn;
    static max(max: number): ValidatorFn;
}
export declare function isPresent(obj: any): boolean;
