export declare class PublishEvents {
    static readonly Unauthorized: string;
    static readonly Error: string;
    static readonly Online: string;
    static readonly Offline: string;
    static readonly UserLogin: string;
    static readonly UserLogout: string;
    static readonly FullSync: string;
    static readonly FullSyncComplete: string;
    static readonly DisplayLoading: string;
    static readonly DismissLoading: string;
    static readonly DisableDefaultLoading: string;
    static readonly EnableDefaultLoading: string;
    static readonly LiveSalesUpdateSalesOrder: string;
    static readonly LiveSalesOrderUpdated: string;
    static readonly LiveSalesEditItemDetailsUpdated: string;
    static readonly LiveSalesFinishedItemDetailEdit: string;
}
