"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DialogResult;
(function (DialogResult) {
    DialogResult[DialogResult["OK"] = 0] = "OK";
    DialogResult[DialogResult["CANCEL"] = 1] = "CANCEL";
})(DialogResult = exports.DialogResult || (exports.DialogResult = {}));
//# sourceMappingURL=dialog-result.js.map