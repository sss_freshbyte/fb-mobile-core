"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PublishEvents = (function () {
    function PublishEvents() {
    }
    Object.defineProperty(PublishEvents, "Unauthorized", {
        get: function () {
            return 'Unauthorized';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "Error", {
        get: function () {
            return 'Error';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "Online", {
        get: function () {
            return 'Online';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "Offline", {
        get: function () {
            return 'Offline';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "UserLogin", {
        get: function () {
            return 'UserLogin';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "UserLogout", {
        get: function () {
            return 'UserLogout';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "FullSync", {
        get: function () {
            return 'FullSyncRequest';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "FullSyncComplete", {
        get: function () {
            return 'FullSyncComplete';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "DisplayLoading", {
        get: function () {
            return 'DisplayLoading';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "DismissLoading", {
        get: function () {
            return 'DismissLoading';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "DisableDefaultLoading", {
        get: function () {
            return 'DisableDefaultLoading';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "EnableDefaultLoading", {
        get: function () {
            return 'EnableDefaultLoading';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "LiveSalesUpdateSalesOrder", {
        get: function () {
            return 'LiveSalesUpdateSalesOrder';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "LiveSalesOrderUpdated", {
        get: function () {
            return 'LiveSalesOrderUpdated';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "LiveSalesEditItemDetailsUpdated", {
        get: function () {
            return 'LiveSalesEditItemDetailsUpdated';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PublishEvents, "LiveSalesFinishedItemDetailEdit", {
        get: function () {
            return 'LiveSalesFinishedItemDetailEdit';
        },
        enumerable: true,
        configurable: true
    });
    return PublishEvents;
}());
exports.PublishEvents = PublishEvents;
//# sourceMappingURL=publish-events.js.map