export declare class QueryBuilder {
    private _query;
    readonly query: string;
    private _queryArgs;
    readonly queryArgs: string[];
    constructor(query: string, args: string[]);
}
