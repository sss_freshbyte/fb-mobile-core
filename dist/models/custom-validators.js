"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.min = function (min) {
        return function (control) {
            if (isPresent(forms_1.Validators.required(control)))
                return null;
            var v = control.value;
            return v >= min ? null : { 'min': true };
        };
    };
    CustomValidators.max = function (max) {
        return function (control) {
            if (isPresent(forms_1.Validators.required(control)))
                return null;
            var v = control.value;
            return v <= max ? null : { 'max': true };
        };
    };
    return CustomValidators;
}());
exports.CustomValidators = CustomValidators;
function isPresent(obj) {
    'use strict';
    return obj !== undefined && obj !== null;
}
exports.isPresent = isPresent;
//# sourceMappingURL=custom-validators.js.map