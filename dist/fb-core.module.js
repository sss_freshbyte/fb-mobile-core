"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ionic_angular_1 = require("ionic-angular");
var google_analytics_1 = require("@ionic-native/google-analytics");
var keyboard_1 = require("@ionic-native/keyboard");
var network_1 = require("@ionic-native/network");
var sqlite_1 = require("@ionic-native/sqlite");
var ng2_translate_1 = require("ng2-translate");
var components_1 = require("./components");
var pipes_1 = require("./pipes");
var providers_1 = require("./providers");
var components_2 = require("./components");
var FbCoreModule = (function () {
    function FbCoreModule() {
    }
    return FbCoreModule;
}());
FbCoreModule.decorators = [
    { type: core_1.NgModule, args: [{
                declarations: [components_1.CORE_COMPONENTS, pipes_1.CORE_PIPES, components_2.PopMenu],
                providers: [providers_1.CORE_PROVIDERS, google_analytics_1.GoogleAnalytics, keyboard_1.Keyboard, network_1.Network, sqlite_1.SQLite],
                exports: [components_1.CORE_COMPONENTS, pipes_1.CORE_PIPES, components_2.PopMenu],
                imports: [http_1.HttpModule, ng2_translate_1.TranslateModule, ionic_angular_1.IonicModule],
                entryComponents: [ionic_angular_1.IonicApp, components_2.PopMenu]
            },] },
];
FbCoreModule.ctorParameters = function () { return []; };
exports.FbCoreModule = FbCoreModule;
//# sourceMappingURL=fb-core.module.js.map