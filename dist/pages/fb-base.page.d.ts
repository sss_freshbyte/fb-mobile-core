import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
export declare class FbBasePage implements OnDestroy {
    protected ngUnsubscribe: Subject<void>;
    protected subscriptions: Subscription[];
    protected i18n: any;
    ionViewDidLeave(): void;
    highlight(e: any): void;
    ngOnDestroy(): void;
}
