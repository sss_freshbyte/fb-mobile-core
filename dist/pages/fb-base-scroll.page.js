"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var fb_base_page_1 = require("./fb-base.page");
var FbBaseScrollPage = (function (_super) {
    __extends(FbBaseScrollPage, _super);
    function FbBaseScrollPage() {
        var _this = _super.call(this) || this;
        _this.preFilterPredicate = function (T) {
            return true;
        };
        _this.initList();
        return _this;
    }
    FbBaseScrollPage.prototype.initList = function () {
        this.itemList = [];
    };
    FbBaseScrollPage.prototype.loadMore = function (observable, target, infiniteScroll, take) {
        var _this = this;
        if (take === void 0) { take = 30; }
        this.subscriptions.push(observable.filter(function (item) {
            var p = _this.preFilterPredicate(item);
            var q = !_this.searchQuery ? true : _this.searchPredicate(item);
            return p && q;
        })
            .skip(target.length)
            .take(take)
            .subscribe(function (item) {
            target.push(item);
        }, function (err) {
            _this._logger.error(err);
        }, function () {
            if (infiniteScroll) {
                infiniteScroll.complete();
            }
        }));
    };
    return FbBaseScrollPage;
}(fb_base_page_1.FbBasePage));
exports.FbBaseScrollPage = FbBaseScrollPage;
//# sourceMappingURL=fb-base-scroll.page.js.map