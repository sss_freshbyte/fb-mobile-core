"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Subject_1 = require("rxjs/Subject");
var FbBasePage = (function () {
    function FbBasePage() {
        this.ngUnsubscribe = new Subject_1.Subject();
        this.subscriptions = [];
        this.i18n = {};
    }
    FbBasePage.prototype.ionViewDidLeave = function () {
        this.subscriptions.forEach(function (subscription) {
            subscription.unsubscribe();
        });
    };
    FbBasePage.prototype.highlight = function (e) {
        e.target.select();
    };
    FbBasePage.prototype.ngOnDestroy = function () {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    };
    return FbBasePage;
}());
exports.FbBasePage = FbBasePage;
//# sourceMappingURL=fb-base.page.js.map