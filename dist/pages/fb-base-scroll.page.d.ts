import { Observable } from 'rxjs/Observable';
import { FbBasePage } from './fb-base.page';
import { LoggerService } from '../providers/logger.service';
export declare class FbBaseScrollPage<T> extends FbBasePage {
    itemList: T[];
    searchQuery: string;
    searchPredicate: Function;
    preFilterPredicate: Function;
    _logger: LoggerService;
    constructor();
    initList(): void;
    loadMore(observable: Observable<T>, target: T[], infiniteScroll?: any, take?: number): void;
}
