import { Keyboard } from '@ionic-native/keyboard';
export declare class KeyboardPop {
    private _keyboard;
    constructor(_keyboard: Keyboard);
    toggleKeyboard(): void;
    close(): void;
    show(): void;
}
