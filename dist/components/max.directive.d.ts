import { OnInit } from '@angular/core';
import { Validator, AbstractControl } from '@angular/forms';
export declare class MaxValidator implements Validator, OnInit {
    max: number;
    private validator;
    ngOnInit(): any;
    validate(c: AbstractControl): {
        [key: string]: any;
    };
}
