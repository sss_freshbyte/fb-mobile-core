"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var FbNumericDirective = (function () {
    function FbNumericDirective(model) {
        this.model = model;
    }
    FbNumericDirective.prototype.onInputChange = function (value) {
        var _value;
        if (value) {
            _value = this.allowDecimal ? value : Math.round(value);
        }
        this.model.valueAccessor.writeValue(_value);
    };
    return FbNumericDirective;
}());
FbNumericDirective.decorators = [
    { type: core_1.Directive, args: [{
                selector: '[ngModel][fbNumeric]',
                providers: [forms_1.NgModel],
                host: {
                    '(ngModelChange)': 'onInputChange($event)'
                }
            },] },
];
FbNumericDirective.ctorParameters = function () { return [
    { type: forms_1.NgModel, },
]; };
FbNumericDirective.propDecorators = {
    'allowDecimal': [{ type: core_1.Input },],
};
exports.FbNumericDirective = FbNumericDirective;
//# sourceMappingURL=fb-numeric.directive.js.map