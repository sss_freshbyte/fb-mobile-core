"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var PopMenu = (function () {
    function PopMenu(_viewController, _navParams) {
        this._viewController = _viewController;
        this._navParams = _navParams;
        this.title = _navParams.get('title');
        this.buttons = _navParams.get('buttons');
    }
    PopMenu.prototype.fire = function (button) {
        return this._viewController.dismiss()
            .then(function () {
            button.handler();
        });
    };
    return PopMenu;
}());
PopMenu.decorators = [
    { type: core_1.Component, args: [{
                template: "\n\t<ion-list>\n\t\t<ion-list-header>{{ title }}</ion-list-header>\n\t\t<button ion-item *ngFor=\"let b of buttons\" (click)=\"fire(b)\">\n\t\t\t<ion-icon name=\"{{ b.icon }}\" *ngIf=\"b.cssClass === undefined\"></ion-icon>\n\t\t\t<ion-icon name=\"{{ b.icon }}\" *ngIf=\"b.cssClass === 'primary'\" color=\"primary\"></ion-icon>\n\t\t\t<ion-icon name=\"{{ b.icon }}\" *ngIf=\"b.cssClass === 'secondary'\" color=\"secondary\"></ion-icon>\n\t\t\t<ion-icon name=\"{{ b.icon }}\" *ngIf=\"b.cssClass === 'danger'\" color=\"danger\"></ion-icon>\n\t\t\t{{ b.text }}\n\t\t</button>\n\t</ion-list>"
            },] },
];
PopMenu.ctorParameters = function () { return [
    { type: ionic_angular_1.ViewController, },
    { type: ionic_angular_1.NavParams, },
]; };
exports.PopMenu = PopMenu;
//# sourceMappingURL=pop-menu.component.js.map