import { NgModel } from '@angular/forms';
export declare class FbNumericDirective {
    private model;
    allowDecimal: boolean;
    constructor(model: NgModel);
    onInputChange(value?: any): any;
}
