"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var keyboard_1 = require("@ionic-native/keyboard");
var KeyboardPop = (function () {
    function KeyboardPop(_keyboard) {
        this._keyboard = _keyboard;
    }
    KeyboardPop.prototype.toggleKeyboard = function () {
        throw new Error('No longer supported');
    };
    KeyboardPop.prototype.close = function () {
        return this._keyboard.close();
    };
    KeyboardPop.prototype.show = function () {
        return this._keyboard.show();
    };
    return KeyboardPop;
}());
KeyboardPop.decorators = [
    { type: core_1.Component, args: [{
                selector: 'keyboard-pop',
                template: '<button class="keyboard-button"><ion-icon name="keypad" (click)="toggleKeyboard()"></ion-icon></button>',
            },] },
];
KeyboardPop.ctorParameters = function () { return [
    { type: keyboard_1.Keyboard, },
]; };
exports.KeyboardPop = KeyboardPop;
//# sourceMappingURL=keyboard-pop.directive.js.map