import { ViewController, NavParams } from 'ionic-angular';
export declare class PopMenu {
    private _viewController;
    private _navParams;
    constructor(_viewController: ViewController, _navParams: NavParams);
    title: string;
    buttons: Array<any>;
    fire(button: any): Promise<void>;
}
