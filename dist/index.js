"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./providers"));
__export(require("./pages"));
__export(require("./models"));
__export(require("./components"));
var fb_core_module_1 = require("./fb-core.module");
exports.FbCoreModule = fb_core_module_1.FbCoreModule;
//# sourceMappingURL=index.js.map