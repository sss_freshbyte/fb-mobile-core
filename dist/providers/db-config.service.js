"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DbConfig = (function () {
    function DbConfig() {
    }
    Object.defineProperty(DbConfig.prototype, "tables", {
        get: function () {
            return this._tables;
        },
        enumerable: true,
        configurable: true
    });
    return DbConfig;
}());
DbConfig.decorators = [
    { type: core_1.Injectable },
];
DbConfig.ctorParameters = function () { return []; };
exports.DbConfig = DbConfig;
//# sourceMappingURL=db-config.service.js.map