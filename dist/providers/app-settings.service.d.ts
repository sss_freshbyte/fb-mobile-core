import { LoggerService } from './logger.service';
import { LocalDb } from './local-db.service';
export declare class AppSettingsService {
    private _logger;
    private _db;
    private _window;
    constructor(_logger: LoggerService, _db: LocalDb);
    readonly ApiUrl: string;
    private _authToken;
    authToken: string;
    private _savedApiUrl;
    readonly SavedApiUrl: string;
    private _binaryVersion;
    readonly BinaryVersion: string;
    load(): Promise<any>;
    loadApiUrl(): Promise<any>;
    setApiUrl(url: string): Promise<any>;
    setSession(session: any): Promise<any>;
    getSession(): Promise<any>;
    resetSession(): Promise<any>;
    buildUrl(url: string): string;
    getUserName(): Promise<string>;
    getAuthToken(): Promise<string>;
}
