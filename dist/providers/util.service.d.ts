import { DateService } from './date.service';
export declare class UtilService {
    private _dateSrvc;
    constructor(_dateSrvc: DateService);
    date(addDays?: number): string;
    static now(addDays?: number): string;
    year(addYears?: number): number;
    static toBool(value: any): boolean;
}
