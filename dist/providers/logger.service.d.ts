import { ToastController } from 'ionic-angular';
export declare class LoggerService {
    private _toastCtrl;
    constructor(_toastCtrl: ToastController);
    log(...args: any[]): void;
    error(message: string, data?: any): void;
    success(message: string): void;
    warning(message: string, data?: any): void;
    private toast(message, cssClass?);
}
