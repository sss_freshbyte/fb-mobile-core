"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var network_1 = require("@ionic-native/network");
var ionic_angular_1 = require("ionic-angular");
var publish_events_1 = require("../models/publish-events");
var ConnectivityService = (function () {
    function ConnectivityService(_events, _network) {
        var _this = this;
        this._events = _events;
        this._network = _network;
        this.eventPublishEnabled = true;
        this._window = window;
        if (this._window.cordova) {
            if (navigator && navigator.connection) {
                this._isConnected = navigator.connection.type !== 'none';
            }
        }
        else {
            this._isConnected = true;
        }
        this.disconnectSubscription = this._network.onDisconnect().subscribe(function () {
            _this._isConnected = false;
            if (_this.eventPublishEnabled) {
                _this._events.publish(publish_events_1.PublishEvents.Offline);
            }
        });
        this.connectSubscription = this._network.onConnect().subscribe(function () {
            _this._isConnected = true;
            if (_this.eventPublishEnabled) {
                setTimeout(function () {
                    _this._events.publish(publish_events_1.PublishEvents.Online);
                }, 2000);
            }
        });
    }
    ConnectivityService.prototype.enableConnectionMonitoring = function () {
        this.eventPublishEnabled = true;
    };
    ConnectivityService.prototype.disableConnectionMonitoring = function () {
        this.eventPublishEnabled = false;
    };
    Object.defineProperty(ConnectivityService.prototype, "isConnected", {
        get: function () {
            return this._isConnected;
        },
        enumerable: true,
        configurable: true
    });
    return ConnectivityService;
}());
ConnectivityService.decorators = [
    { type: core_1.Injectable },
];
ConnectivityService.ctorParameters = function () { return [
    { type: ionic_angular_1.Events, },
    { type: network_1.Network, },
]; };
exports.ConnectivityService = ConnectivityService;
//# sourceMappingURL=connectivity.service.js.map