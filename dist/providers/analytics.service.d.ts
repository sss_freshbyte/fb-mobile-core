import { Platform } from 'ionic-angular';
import { LoggerService } from './logger.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
export declare class AnalyticsService {
    private _ga;
    constructor(platform: Platform, logger: LoggerService, _ga: GoogleAnalytics);
    trackException(description: string, isFatal: boolean): Promise<void>;
    trackView(screen: string, campaignUrl?: string, newSession?: boolean): Promise<void>;
    trackEvent(category: string, action: string, label?: string, value?: number, newSession?: boolean): Promise<void>;
    trackMetric(key: string, value?: string): Promise<void>;
    trackTiming(category: string, intervalMilliseconds: number, variable?: string, label?: string): Promise<void>;
    addTransaction(id: string, affiliation: string, revenue: number, tax: number, shipping: number, currencyCode: string): Promise<void>;
    addTransactionItem(id: string, name: string, sku: string, category: string, price: number, quantity: number, currencyCode: string): Promise<void>;
    addCustomerDimension(key: number, value?: string): Promise<void>;
    setUserId(value: string): Promise<void>;
    setAppVersion(value: string): Promise<void>;
    setAnonymousIP(value: boolean): Promise<void>;
    setOptOut(value: boolean): Promise<void>;
    setAllowIDFCollection(value: boolean): Promise<void>;
    enableUncaughtExceptionReporting(enabled: boolean): Promise<void>;
    debugMode(): Promise<void>;
}
