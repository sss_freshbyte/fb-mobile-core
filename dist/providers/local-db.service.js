"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var storage_1 = require("@ionic/storage");
var rdb_store_1 = require("./rdb-store");
var logger_service_1 = require("./logger.service");
var db_config_service_1 = require("./db-config.service");
var query_builder_service_1 = require("./query-builder.service");
var LocalDb = (function () {
    function LocalDb(_logger, _dbConfig, _rdbStore, _queryBuilder, _kvStore) {
        this._logger = _logger;
        this._dbConfig = _dbConfig;
        this._rdbStore = _rdbStore;
        this._queryBuilder = _queryBuilder;
        this._kvStore = _kvStore;
        this._rdbStore.init();
    }
    LocalDb.prototype.queryWithBuilder = function (queryBuilder) {
        return this._rdbStore.query(queryBuilder.query, queryBuilder.queryArgs);
    };
    LocalDb.prototype.query = function (query, params) {
        return this._rdbStore.query(query, params);
    };
    LocalDb.prototype.queryWithArrayResult = function (query, params) {
        return this._rdbStore.queryWithArrayResult(query, params);
    };
    LocalDb.prototype.get = function (key) {
        return this._kvStore.get(key)
            .then(function (results) {
            if (results && results[0] === '{') {
                return JSON.parse(results);
            }
            else {
                return results;
            }
        });
    };
    LocalDb.prototype.set = function (key, value) {
        if (value instanceof Object) {
            value = JSON.stringify(value);
        }
        return this._kvStore.set(key, value);
    };
    LocalDb.prototype.allKeys = function () {
        return this._kvStore.keys();
    };
    LocalDb.prototype.removeKvp = function (key) {
        return this._kvStore.remove(key);
    };
    LocalDb.prototype.removeAllKvp = function () {
        return this._kvStore.clear();
    };
    LocalDb.prototype.forEachKvp = function (callback) {
        return this._kvStore.forEach(callback);
    };
    LocalDb.prototype.resetDb = function (tables, reset) {
        var _this = this;
        if (reset === void 0) { reset = false; }
        return Promise.all(tables
            .filter(function (tables) { return reset; })
            .map(function (table) { return _this.queryWithBuilder(_this._queryBuilder.drop(table.name)); }));
    };
    LocalDb.prototype.setupDb = function (reset) {
        var _this = this;
        return this.resetDb(this._dbConfig.tables, reset)
            .then(function () { return Promise.all(_this._dbConfig.tables
            .filter(function (table) { return table.isObject !== true; })
            .map(function (table) { return _this.queryWithBuilder(_this._queryBuilder.create(table)); })); });
    };
    LocalDb.prototype.dropAndCreate = function (tableName) {
        var _this = this;
        return Promise.all(this._dbConfig.tables
            .filter(function (tbl) { return (tbl.name === tableName) && (tbl.isObject !== true); })
            .map(function (tbl) { return _this.queryWithBuilder(_this._queryBuilder.drop(tbl.name))
            .then(function () { return _this.queryWithBuilder(_this._queryBuilder.create(tbl)); }); }));
    };
    LocalDb.prototype.seedTable = function (tableName, rows) {
        var _this = this;
        return Promise.all(this._dbConfig.tables
            .filter(function (tbl) { return tbl.name === tableName; })
            .map(function (t) { return t.isObject === true ? _this.set(t.name, rows[0]) : _this._rdbStore.transaction(t, rows, _this._queryBuilder.insert); }));
    };
    LocalDb.prototype.insert = function (tableName, model) {
        var table = this._dbConfig.tables.find(function (tbl) { return tbl.name === tableName; });
        var qb = this._queryBuilder.update(table, model);
        return this.queryWithBuilder(qb);
    };
    return LocalDb;
}());
LocalDb.decorators = [
    { type: core_1.Injectable },
];
LocalDb.ctorParameters = function () { return [
    { type: logger_service_1.LoggerService, },
    { type: db_config_service_1.DbConfig, },
    { type: rdb_store_1.RdbStore, },
    { type: query_builder_service_1.QueryBuilderService, },
    { type: storage_1.Storage, },
]; };
exports.LocalDb = LocalDb;
//# sourceMappingURL=local-db.service.js.map