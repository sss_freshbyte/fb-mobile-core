"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ionic_angular_1 = require("ionic-angular");
var rxjs_1 = require("rxjs");
require("rxjs/add/operator/map");
var app_settings_service_1 = require("./app-settings.service");
var publish_events_1 = require("../models/publish-events");
var logger_service_1 = require("./logger.service");
var Api = (function () {
    function Api(_http, _appSettingsSrvc, _events, _logger) {
        this._http = _http;
        this._appSettingsSrvc = _appSettingsSrvc;
        this._events = _events;
        this._logger = _logger;
    }
    Api.prototype.resourceUrl = function (urlSegment) {
        return this._appSettingsSrvc.ApiUrl + '/' + urlSegment + '/';
    };
    Api.prototype.buildParams = function (params, options) {
        if (params) {
            var p = new http_1.URLSearchParams();
            for (var k in params) {
                p.set(k, params[k]);
            }
            options.search = !options.search && p || options.search;
        }
    };
    Api.prototype.get$ = function (endpoint, params, options) {
        var _this = this;
        this.notifyLoad();
        if (!options) {
            options = new http_1.RequestOptions();
        }
        this.buildParams(params, options);
        return this._http.get(this.resourceUrl(endpoint), options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .catch(function (err) { return _this.handleError(err); });
    };
    Api.prototype.get = function (endpoint, params, options) {
        var _this = this;
        this.notifyLoad();
        return this.get$(endpoint, params, options)
            .toPromise()
            .then(function (result) { return _this.notifyDismiss(result); });
    };
    Api.prototype.post$ = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this._http.post(this.resourceUrl(endpoint), body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .catch(function (err) { return _this.handleError(err); });
    };
    Api.prototype.post = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this.post$(endpoint, body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .toPromise();
    };
    Api.prototype.put$ = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this._http.put(this.resourceUrl(endpoint), body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .catch(function (err) { return _this.handleError(err); });
    };
    Api.prototype.put = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this.put$(endpoint, body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .toPromise();
    };
    Api.prototype.delete$ = function (endpoint, options) {
        var _this = this;
        this.notifyLoad();
        return this._http.delete(this.resourceUrl(endpoint), options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .catch(function (err) { return _this.handleError(err); });
    };
    Api.prototype.delete = function (endpoint, options) {
        var _this = this;
        this.notifyLoad();
        return this.delete$(endpoint, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .toPromise();
    };
    Api.prototype.patch$ = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this._http.patch(this.resourceUrl(endpoint), body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .catch(function (err) { return _this.handleError(err); });
    };
    Api.prototype.patch = function (endpoint, body, options) {
        var _this = this;
        this.notifyLoad();
        return this.patch$(endpoint, body, options)
            .map(function (result) { return _this.notifyDismiss(result); })
            .toPromise();
    };
    Api.prototype.notifyLoad = function () {
        this._events.publish(publish_events_1.PublishEvents.DisplayLoading);
    };
    Api.prototype.notifyDismiss = function (result) {
        this._events.publish(publish_events_1.PublishEvents.DismissLoading);
        return result;
    };
    Api.prototype.handleError = function (error) {
        var errorMessage = '';
        if (error.message) {
            errorMessage = error.message;
        }
        else if (error._body) {
            try {
                errorMessage = JSON.parse(error._body).message;
            }
            catch (e) {
                errorMessage = '';
            }
        }
        var msg = (errorMessage) ? errorMessage :
            error.status ? error.status + ' - ' + error.statusText : 'Server error';
        this._logger.error(msg);
        this._events.publish(publish_events_1.PublishEvents.Error, msg, error.status);
        return rxjs_1.Observable.throw(msg);
    };
    return Api;
}());
Api.decorators = [
    { type: core_1.Injectable },
];
Api.ctorParameters = function () { return [
    { type: http_1.Http, },
    { type: app_settings_service_1.AppSettingsService, },
    { type: ionic_angular_1.Events, },
    { type: logger_service_1.LoggerService, },
]; };
exports.Api = Api;
//# sourceMappingURL=api.service.js.map