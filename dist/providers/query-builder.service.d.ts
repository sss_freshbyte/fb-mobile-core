import { QueryBuilder } from '../models/query-builder';
import { IDbTable } from '../models/db-table';
export declare class QueryBuilderService {
    select(tableName: string, pargs?: [[string, any]]): QueryBuilder;
    drop(tableName: string): QueryBuilder;
    create(table: IDbTable): QueryBuilder;
    insert(table: IDbTable, row: any): QueryBuilder;
    update(table: IDbTable, model: any): QueryBuilder;
}
