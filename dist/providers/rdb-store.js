"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sqlite_1 = require("@ionic-native/sqlite");
var logger_service_1 = require("./logger.service");
var core_1 = require("@angular/core");
var DB_NAME = '__edibleMobile';
var win = window;
var RdbStore = (function () {
    function RdbStore(_logger) {
        this._logger = _logger;
        this._db = null;
    }
    RdbStore.prototype.init = function () {
        return this.openConnection();
    };
    RdbStore.prototype.openConnection = function () {
        var _this = this;
        if (this._db !== null) {
            return Promise.resolve();
        }
        if (win.sqlitePlugin) {
            var db = new sqlite_1.SQLite();
            return db.create({
                name: DB_NAME,
                location: 'default'
            })
                .then(function (db) { return _this._db = db; });
        }
        else {
            console.warn('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');
            this._db = win.openDatabase(DB_NAME, '1.0', 'database', 5 * 1024 * 1024);
            return Promise.resolve();
        }
    };
    RdbStore.prototype.query = function (query, params) {
        var _this = this;
        if (params === void 0) { params = []; }
        return new Promise(function (resolve, reject) {
            try {
                return _this.openConnection().then(function () {
                    return _this._db.transaction(function (tx) {
                        tx.executeSql(query, params, function (innerTx, res) { return resolve({ tx: innerTx, res: res }); }, function (innerTx, err) { return reject({ tx: innerTx, err: err }); });
                    }, function (err) { return reject({ err: err }); });
                });
            }
            catch (err) {
                reject({ err: err });
            }
        });
    };
    RdbStore.prototype.queryWithArrayResult = function (query, params) {
        return this.query(query, params)
            .then(function (results) {
            var final = [];
            for (var i = 0; i < results.res.rows.length; i++) {
                final.push(results.res.rows.item(i));
            }
            return final;
        });
    };
    RdbStore.prototype.transaction = function (table, rows, callback) {
        var _this = this;
        return this._db.transaction(function (tx) { return Promise.all(rows.map(function (row) {
            var qb = callback(table, row);
            return tx.executeSql(qb.query, qb.queryArgs);
        })); }, function (err) { return _this._logger.error(err); }, this._logger.log('sync for "' + table.name + '" with ' + rows.length + ' rows completed'));
    };
    return RdbStore;
}());
RdbStore.decorators = [
    { type: core_1.Injectable },
];
RdbStore.ctorParameters = function () { return [
    { type: logger_service_1.LoggerService, },
]; };
exports.RdbStore = RdbStore;
//# sourceMappingURL=rdb-store.js.map