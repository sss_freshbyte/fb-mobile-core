"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DateService = (function () {
    function DateService() {
    }
    DateService.dateWithoutTime = function () {
        var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    };
    DateService.maxDateForPicker = function () {
        var d = new Date(), nextYear = d.getFullYear() + 1;
        return nextYear;
    };
    DateService.prototype.now = function () {
        return new Date();
    };
    return DateService;
}());
DateService.decorators = [
    { type: core_1.Injectable },
];
DateService.ctorParameters = function () { return []; };
exports.DateService = DateService;
//# sourceMappingURL=date.service.js.map