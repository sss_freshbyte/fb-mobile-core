import { Network } from '@ionic-native/network';
import { Events } from 'ionic-angular';
export declare class ConnectivityService {
    private _events;
    private _network;
    private _window;
    private disconnectSubscription;
    private connectSubscription;
    private eventPublishEnabled;
    constructor(_events: Events, _network: Network);
    enableConnectionMonitoring(): void;
    disableConnectionMonitoring(): void;
    private _isConnected;
    readonly isConnected: boolean;
}
