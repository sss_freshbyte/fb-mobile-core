import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { Response, RequestOptions, ConnectionBackend } from '@angular/http';
import { Events } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { AppSettingsService } from './app-settings.service';
import { LoggerService } from './logger.service';
export declare class FbHttp extends Http {
    protected _backend: ConnectionBackend;
    protected _defaultOptions: RequestOptions;
    protected _appSettingsService: AppSettingsService;
    protected _events: Events;
    protected _logger: LoggerService;
    constructor(_backend: ConnectionBackend, _defaultOptions: RequestOptions, _appSettingsService: AppSettingsService, _events: Events, _logger: LoggerService);
    buildHeaders(): Headers;
    fbRequestOptions(originalOptions: RequestOptionsArgs): RequestOptions;
    request(url: string, options?: RequestOptionsArgs): Observable<Response>;
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, options?: RequestOptionsArgs): Observable<Response>;
    patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response>;
    head(url: string, options?: RequestOptionsArgs): Observable<Response>;
}
