import { Storage } from '@ionic/storage';
import { RdbStore } from './rdb-store';
import { LoggerService } from './logger.service';
import { DbConfig } from './db-config.service';
import { QueryBuilderService } from './query-builder.service';
import { QueryBuilder } from '../models/query-builder';
export declare class LocalDb {
    private _logger;
    private _dbConfig;
    private _rdbStore;
    private _queryBuilder;
    private _kvStore;
    constructor(_logger: LoggerService, _dbConfig: DbConfig, _rdbStore: RdbStore, _queryBuilder: QueryBuilderService, _kvStore: Storage);
    queryWithBuilder(queryBuilder: QueryBuilder): Promise<any>;
    query(query: string, params?: any[]): Promise<any>;
    queryWithArrayResult(query: string, params?: any[]): Promise<Array<any>>;
    get(key: string): Promise<any>;
    set(key: string, value: any): Promise<any>;
    allKeys(): Promise<any>;
    removeKvp(key: string): Promise<void>;
    removeAllKvp(): Promise<void>;
    forEachKvp(callback: (value: any, key: string, index) => Promise<any>): Promise<void>;
    private resetDb(tables, reset?);
    setupDb(reset?: boolean): Promise<any>;
    dropAndCreate(tableName: string): Promise<any>;
    seedTable(tableName: string, rows: any[]): Promise<any>;
    insert(tableName: string, model: any): Promise<any>;
}
