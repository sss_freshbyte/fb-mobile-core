import { Api } from './api.service';
import { LocalDb } from './local-db.service';
export declare class TableSyncService {
    private _api;
    private _db;
    private _resource;
    constructor(_api: Api, _db: LocalDb);
    sync(tableName: string, startId: any): Promise<any>;
}
