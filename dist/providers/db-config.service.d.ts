import { IDbTable } from '../models';
export declare class DbConfig {
    private _tables;
    readonly tables: Array<IDbTable>;
}
