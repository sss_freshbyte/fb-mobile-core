"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var logger_service_1 = require("./logger.service");
var google_analytics_1 = require("@ionic-native/google-analytics");
var AnalyticsService = (function () {
    function AnalyticsService(platform, logger, _ga) {
        var _this = this;
        this._ga = _ga;
        platform.ready().then(function () { return _this._ga.startTrackerWithId('UA-84866094-2'); })
            .then(function () { return logger.success('Google Analytics Initialized'); })
            .catch(function (e) { return logger.log('Error starting Google Analytics', e); });
    }
    AnalyticsService.prototype.trackException = function (description, isFatal) {
        return this._ga.trackException(description, isFatal);
    };
    AnalyticsService.prototype.trackView = function (screen, campaignUrl, newSession) {
        return this._ga.trackView(screen, campaignUrl, newSession || true);
    };
    AnalyticsService.prototype.trackEvent = function (category, action, label, value, newSession) {
        return this._ga.trackEvent(category, action, label, value, newSession || true);
    };
    AnalyticsService.prototype.trackMetric = function (key, value) {
        return this._ga.trackMetric(key, value);
    };
    AnalyticsService.prototype.trackTiming = function (category, intervalMilliseconds, variable, label) {
        return this._ga.trackTiming(category, intervalMilliseconds, variable, label);
    };
    AnalyticsService.prototype.addTransaction = function (id, affiliation, revenue, tax, shipping, currencyCode) {
        return this._ga.addTransaction(id, affiliation, revenue, tax, shipping, currencyCode);
    };
    AnalyticsService.prototype.addTransactionItem = function (id, name, sku, category, price, quantity, currencyCode) {
        return this._ga.addTransactionItem(id, name, sku, category, price, quantity, currencyCode);
    };
    AnalyticsService.prototype.addCustomerDimension = function (key, value) {
        return this._ga.addCustomDimension(key, value);
    };
    AnalyticsService.prototype.setUserId = function (value) {
        return this._ga.setUserId(value);
    };
    AnalyticsService.prototype.setAppVersion = function (value) {
        return this._ga.setAppVersion(value);
    };
    AnalyticsService.prototype.setAnonymousIP = function (value) {
        return this._ga.setAnonymizeIp(value);
    };
    AnalyticsService.prototype.setOptOut = function (value) {
        return this._ga.setOptOut(value);
    };
    AnalyticsService.prototype.setAllowIDFCollection = function (value) {
        return this._ga.setAllowIDFACollection(value);
    };
    AnalyticsService.prototype.enableUncaughtExceptionReporting = function (enabled) {
        return this._ga.enableUncaughtExceptionReporting(enabled);
    };
    AnalyticsService.prototype.debugMode = function () {
        return this._ga.debugMode();
    };
    return AnalyticsService;
}());
AnalyticsService.decorators = [
    { type: core_1.Injectable },
];
AnalyticsService.ctorParameters = function () { return [
    { type: ionic_angular_1.Platform, },
    { type: logger_service_1.LoggerService, },
    { type: google_analytics_1.GoogleAnalytics, },
]; };
exports.AnalyticsService = AnalyticsService;
//# sourceMappingURL=analytics.service.js.map