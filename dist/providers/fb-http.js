"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var ionic_angular_1 = require("ionic-angular");
var app_settings_service_1 = require("./app-settings.service");
var logger_service_1 = require("./logger.service");
var FbHttp = (function (_super) {
    __extends(FbHttp, _super);
    function FbHttp(_backend, _defaultOptions, _appSettingsService, _events, _logger) {
        var _this = _super.call(this, _backend, _defaultOptions) || this;
        _this._backend = _backend;
        _this._defaultOptions = _defaultOptions;
        _this._appSettingsService = _appSettingsService;
        _this._events = _events;
        _this._logger = _logger;
        return _this;
    }
    FbHttp.prototype.buildHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('AppVersion', this._appSettingsService.BinaryVersion);
        headers.append('Authorization', this._appSettingsService.authToken);
        headers.append('Content-Type', 'application/json');
        return headers;
    };
    FbHttp.prototype.fbRequestOptions = function (originalOptions) {
        var fbOptions = new http_2.RequestOptions({ headers: this.buildHeaders() });
        return fbOptions.merge(originalOptions);
    };
    FbHttp.prototype.request = function (url, options) {
        return _super.prototype.request.call(this, url, this.fbRequestOptions(options));
    };
    FbHttp.prototype.get = function (url, options) {
        return _super.prototype.get.call(this, url, this.fbRequestOptions(options));
    };
    FbHttp.prototype.put = function (url, body, options) {
        return _super.prototype.put.call(this, url, body, this.fbRequestOptions(options));
    };
    FbHttp.prototype.post = function (url, body, options) {
        return _super.prototype.post.call(this, url, body, this.fbRequestOptions(options));
    };
    FbHttp.prototype.delete = function (url, options) {
        return _super.prototype.delete.call(this, url, this.fbRequestOptions(options));
    };
    FbHttp.prototype.patch = function (url, body, options) {
        return _super.prototype.patch.call(this, url, body, this.fbRequestOptions(options));
    };
    FbHttp.prototype.head = function (url, options) {
        return _super.prototype.request.call(this, url, this.fbRequestOptions(options));
    };
    return FbHttp;
}(http_1.Http));
FbHttp.decorators = [
    { type: core_1.Injectable },
];
FbHttp.ctorParameters = function () { return [
    { type: http_2.ConnectionBackend, },
    { type: http_2.RequestOptions, },
    { type: app_settings_service_1.AppSettingsService, },
    { type: ionic_angular_1.Events, },
    { type: logger_service_1.LoggerService, },
]; };
exports.FbHttp = FbHttp;
//# sourceMappingURL=fb-http.js.map