"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var local_db_service_1 = require("./local-db.service");
var TableSyncService = (function () {
    function TableSyncService(_api, _db) {
        this._api = _api;
        this._db = _db;
        this._resource = 'sync';
    }
    TableSyncService.prototype.sync = function (tableName, startId) {
        var _this = this;
        var endpoint = tableName.includes('_') ? tableName.split('_')[1] : tableName;
        return this._db.dropAndCreate(tableName)
            .then(function () { return _this._api.get(_this._resource + '/' + endpoint, { startId: startId }); })
            .then(function (r) { return _this._db.seedTable(tableName, r.json()); });
    };
    return TableSyncService;
}());
TableSyncService.decorators = [
    { type: core_1.Injectable },
];
TableSyncService.ctorParameters = function () { return [
    { type: api_service_1.Api, },
    { type: local_db_service_1.LocalDb, },
]; };
exports.TableSyncService = TableSyncService;
//# sourceMappingURL=table-sync.service.js.map