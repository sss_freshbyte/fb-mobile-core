"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var date_service_1 = require("./date.service");
var UtilService = (function () {
    function UtilService(_dateSrvc) {
        this._dateSrvc = _dateSrvc;
    }
    UtilService.prototype.date = function (addDays) {
        var targetDate = this._dateSrvc.now();
        targetDate.setDate(targetDate.getDate() + (addDays || 0));
        var year = targetDate.getFullYear().toString();
        var mth = ('00' + (targetDate.getMonth() + 1)).slice(-2);
        var day = ('00' + targetDate.getDate()).slice(-2);
        return year + "-" + mth + "-" + day;
    };
    UtilService.now = function (addDays) {
        var targetDate = new Date();
        targetDate.setDate(targetDate.getDate() + (addDays || 0));
        var year = targetDate.getFullYear().toString();
        var mth = ('00' + (targetDate.getMonth() + 1)).slice(-2);
        var day = ('00' + targetDate.getDate()).slice(-2);
        return year + "-" + mth + "-" + day;
    };
    UtilService.prototype.year = function (addYears) {
        var d = this._dateSrvc.now(), yr = d.getFullYear() + (addYears || 0);
        return yr;
    };
    UtilService.toBool = function (value) {
        if (typeof value === 'string') {
            value = value.toLowerCase();
        }
        else if (typeof value === 'boolean') {
            return value;
        }
        switch (value) {
            case 'true':
                return true;
            case 1:
                return true;
            case '1':
                return true;
            default:
                return false;
        }
    };
    return UtilService;
}());
UtilService.decorators = [
    { type: core_1.Injectable },
];
UtilService.ctorParameters = function () { return [
    { type: date_service_1.DateService, },
]; };
exports.UtilService = UtilService;
//# sourceMappingURL=util.service.js.map