"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var LoggerService = (function () {
    function LoggerService(_toastCtrl) {
        this._toastCtrl = _toastCtrl;
    }
    LoggerService.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log(args);
    };
    LoggerService.prototype.error = function (message, data) {
        console.error(message, data);
        this.toast(message, 'toast-error');
    };
    LoggerService.prototype.success = function (message) {
        console.log(message);
        this.toast(message, 'toast-success');
    };
    LoggerService.prototype.warning = function (message, data) {
        console.warn(message, data);
        this.toast(message, 'toast-warn');
    };
    LoggerService.prototype.toast = function (message, cssClass) {
        var t = this._toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top',
            cssClass: cssClass
        });
        return t.present();
    };
    return LoggerService;
}());
LoggerService.decorators = [
    { type: core_1.Injectable },
];
LoggerService.ctorParameters = function () { return [
    { type: ionic_angular_1.ToastController, },
]; };
exports.LoggerService = LoggerService;
//# sourceMappingURL=logger.service.js.map