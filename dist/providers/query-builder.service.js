"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var query_builder_1 = require("../models/query-builder");
var QueryBuilderService = (function () {
    function QueryBuilderService() {
    }
    QueryBuilderService.prototype.select = function (tableName, pargs) {
        var query = 'SELECT * FROM ' + tableName;
        var args = new Array(pargs ? pargs.length : 0);
        if (pargs && pargs.length > 0) {
            query += ' WHERE ';
            for (var i = 0; i < pargs.length; i++) {
                query += pargs[i][0] + ' = ? ';
                if (pargs.length - 1 > i) {
                    query += ' AND ';
                }
                args[i] = pargs[i][1];
            }
        }
        return new query_builder_1.QueryBuilder(query, args);
    };
    QueryBuilderService.prototype.drop = function (tableName) {
        var query = "DROP TABLE IF EXISTS " + tableName;
        return new query_builder_1.QueryBuilder(query, []);
    };
    QueryBuilderService.prototype.create = function (table) {
        var query = 'CREATE TABLE IF NOT EXISTS ' + table.name +
            '(' + table.columns.map(function (c) {
            return c.name + ' ' + c.type;
        }).join(', ');
        query = query.slice(0, query.length - 1) + ')';
        return new query_builder_1.QueryBuilder(query, []);
    };
    QueryBuilderService.prototype.insert = function (table, row) {
        var params = [];
        var query = 'INSERT OR REPLACE INTO ' + table.name +
            ' (' + table.columns.map(function (c) {
            return c.name;
        }).join(', ') + ') VALUES(';
        table.columns.forEach(function (col, j) {
            query += (j === 0 ? '?' : ', ?');
            params.push(row[col.name]);
        });
        query += ')';
        return new query_builder_1.QueryBuilder(query, params);
    };
    QueryBuilderService.prototype.update = function (table, model) {
        var params = [];
        var query = 'INSERT OR REPLACE INTO ' + table.name + ' (';
        var first = true;
        table.columns.forEach(function (col) {
            if (col.name !== 'id' || (col.name === 'id' && model.id > 0)) {
                query += (first ? col.name : ', ' + col.name);
                first = false;
            }
        });
        query += ') VALUES(';
        first = true;
        table.columns.forEach(function (col) {
            if (col.name !== 'id' || (col.name === 'id' && model.id > 0)) {
                query += (first ? '?' : ', ?');
                params.push(model['_' + col.name]);
                first = false;
            }
        });
        query += ')';
        return new query_builder_1.QueryBuilder(query, params);
    };
    return QueryBuilderService;
}());
QueryBuilderService.decorators = [
    { type: core_1.Injectable },
];
QueryBuilderService.ctorParameters = function () { return []; };
exports.QueryBuilderService = QueryBuilderService;
//# sourceMappingURL=query-builder.service.js.map