import { Http, RequestOptions } from '@angular/http';
import { Events } from 'ionic-angular';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { AppSettingsService } from './app-settings.service';
import { LoggerService } from './logger.service';
export declare class Api {
    private _http;
    private _appSettingsSrvc;
    private _events;
    private _logger;
    constructor(_http: Http, _appSettingsSrvc: AppSettingsService, _events: Events, _logger: LoggerService);
    protected resourceUrl(urlSegment?: string): string;
    buildParams(params?: any, options?: RequestOptions): void;
    get$(endpoint: string, params?: any, options?: RequestOptions): Observable<any>;
    get(endpoint: string, params?: any, options?: RequestOptions): Promise<any>;
    post$(endpoint: string, body: any, options?: RequestOptions): Observable<any>;
    post(endpoint: string, body: any, options?: RequestOptions): Promise<any>;
    put$(endpoint: string, body: any, options?: RequestOptions): Observable<any>;
    put(endpoint: string, body: any, options?: RequestOptions): Promise<any>;
    delete$(endpoint: string, options?: RequestOptions): Observable<any>;
    delete(endpoint: string, options?: RequestOptions): Promise<any>;
    patch$(endpoint: string, body: any, options?: RequestOptions): Observable<any>;
    patch(endpoint: string, body: any, options?: RequestOptions): Promise<any>;
    private notifyLoad();
    private notifyDismiss(result);
    protected handleError(error: any): any;
}
