export declare class DateService {
    static dateWithoutTime(): string;
    static maxDateForPicker(): number;
    now(): Date;
}
