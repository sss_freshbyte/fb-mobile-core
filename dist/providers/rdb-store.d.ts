import { IDbTable } from '../models/db-table';
import { QueryBuilder } from '../models/query-builder';
import { LoggerService } from './logger.service';
export declare class RdbStore {
    private _logger;
    private _db;
    constructor(_logger: LoggerService);
    init(): Promise<void>;
    private openConnection();
    query(query: string, params?: any[]): Promise<any>;
    queryWithArrayResult(query: string, params?: any[]): Promise<Array<any>>;
    transaction(table: IDbTable, rows: any[], callback: (t: IDbTable, r: any[]) => QueryBuilder): Promise<any>;
}
