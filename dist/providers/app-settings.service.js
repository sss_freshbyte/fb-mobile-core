"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var logger_service_1 = require("./logger.service");
var local_db_service_1 = require("./local-db.service");
var AppSettingsService = (function () {
    function AppSettingsService(_logger, _db) {
        this._logger = _logger;
        this._db = _db;
        this._window = window;
        this.load();
    }
    Object.defineProperty(AppSettingsService.prototype, "ApiUrl", {
        get: function () {
            return this.buildUrl(this._savedApiUrl);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettingsService.prototype, "authToken", {
        get: function () {
            return this._authToken;
        },
        set: function (value) {
            this._authToken = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettingsService.prototype, "SavedApiUrl", {
        get: function () {
            return this._savedApiUrl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettingsService.prototype, "BinaryVersion", {
        get: function () {
            return this._binaryVersion;
        },
        enumerable: true,
        configurable: true
    });
    AppSettingsService.prototype.load = function () {
        var _this = this;
        return this.loadApiUrl()
            .then(function () {
            if (_this._window.cordova) {
                var cordova = (_this._window.cordova);
                return cordova.getAppVersion.getVersionNumber();
            }
            else {
                return Promise.resolve('Browser');
            }
        })
            .then(function (version) {
            _this._binaryVersion = version;
            return _this.getAuthToken();
        })
            .then(function (token) { return _this._authToken = token; })
            .catch(function (err) { return _this._logger.error(err); });
    };
    AppSettingsService.prototype.loadApiUrl = function () {
        var _this = this;
        return this._db.get('apiUrl')
            .then(function (url) {
            _this._savedApiUrl = url;
        });
    };
    AppSettingsService.prototype.setApiUrl = function (url) {
        var _this = this;
        return this._db.set('apiUrl', url)
            .then(function () {
            _this._savedApiUrl = url;
        });
    };
    AppSettingsService.prototype.setSession = function (session) {
        this.authToken = session.token;
        return this._db.set('session', session);
    };
    AppSettingsService.prototype.getSession = function () {
        return this._db.get('session');
    };
    AppSettingsService.prototype.resetSession = function () {
        return this.setSession({});
    };
    AppSettingsService.prototype.buildUrl = function (url) {
        if (url.indexOf('http') === -1) {
            var result = 'https://' + url + '.edible-online.com/api';
            return result;
        }
        return url;
    };
    AppSettingsService.prototype.getUserName = function () {
        return this._db.get('session')
            .then(function (session) {
            return session.edibleUserName;
        });
    };
    AppSettingsService.prototype.getAuthToken = function () {
        return this._db.get('session')
            .then(function (session) {
            var token = session ? session.token || '' : '';
            return Promise.resolve(token);
        });
    };
    return AppSettingsService;
}());
AppSettingsService.decorators = [
    { type: core_1.Injectable },
];
AppSettingsService.ctorParameters = function () { return [
    { type: logger_service_1.LoggerService, },
    { type: local_db_service_1.LocalDb, },
]; };
exports.AppSettingsService = AppSettingsService;
//# sourceMappingURL=app-settings.service.js.map