"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_service_1 = require("./app-settings.service");
exports.AppSettingsService = app_settings_service_1.AppSettingsService;
var logger_service_1 = require("./logger.service");
exports.LoggerService = logger_service_1.LoggerService;
var local_db_service_1 = require("./local-db.service");
exports.LocalDb = local_db_service_1.LocalDb;
var table_sync_service_1 = require("./table-sync.service");
exports.TableSyncService = table_sync_service_1.TableSyncService;
var util_service_1 = require("./util.service");
exports.UtilService = util_service_1.UtilService;
var fb_http_1 = require("./fb-http");
exports.FbHttp = fb_http_1.FbHttp;
var rdb_store_1 = require("./rdb-store");
exports.RdbStore = rdb_store_1.RdbStore;
var connectivity_service_1 = require("./connectivity.service");
exports.ConnectivityService = connectivity_service_1.ConnectivityService;
var date_service_1 = require("./date.service");
exports.DateService = date_service_1.DateService;
var db_config_service_1 = require("./db-config.service");
exports.DbConfig = db_config_service_1.DbConfig;
var query_builder_service_1 = require("./query-builder.service");
exports.QueryBuilderService = query_builder_service_1.QueryBuilderService;
var api_service_1 = require("./api.service");
exports.Api = api_service_1.Api;
var analytics_service_1 = require("./analytics.service");
exports.AnalyticsService = analytics_service_1.AnalyticsService;
exports.CORE_PROVIDERS = [
    api_service_1.Api,
    logger_service_1.LoggerService,
    local_db_service_1.LocalDb,
    app_settings_service_1.AppSettingsService,
    fb_http_1.FbHttp,
    table_sync_service_1.TableSyncService,
    util_service_1.UtilService,
    rdb_store_1.RdbStore,
    connectivity_service_1.ConnectivityService,
    date_service_1.DateService,
    db_config_service_1.DbConfig,
    query_builder_service_1.QueryBuilderService,
    analytics_service_1.AnalyticsService
];
//# sourceMappingURL=index.js.map