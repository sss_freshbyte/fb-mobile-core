"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SearchPipe = (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (data, _a) {
        var fields = _a[0], term = _a[1];
        var parts = term && term.trim().toLowerCase().split(/\s+/);
        if (!parts || !parts.length)
            return data;
        return data.filter(function (item) {
            return parts.every(function (part) {
                return fields.some(function (field) {
                    return item[field].toLowerCase().indexOf(part) > -1;
                });
            });
        });
    };
    return SearchPipe;
}());
SearchPipe.decorators = [
    { type: core_1.Injectable },
    { type: core_1.Pipe, args: [{ name: 'search', pure: false },] },
];
SearchPipe.ctorParameters = function () { return []; };
exports.SearchPipe = SearchPipe;
//# sourceMappingURL=search.pipe.js.map