import { PipeTransform } from '@angular/core';
export declare class SearchPipe implements PipeTransform {
    transform(data: any[], [fields, term]: [string[], string]): any[];
}
