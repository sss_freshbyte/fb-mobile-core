"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var order_by_pipe_1 = require("./order-by.pipe");
exports.OrderByPipe = order_by_pipe_1.OrderByPipe;
var search_pipe_1 = require("./search.pipe");
exports.SearchPipe = search_pipe_1.SearchPipe;
var filter_pipe_1 = require("./filter.pipe");
exports.FilterPipe = filter_pipe_1.FilterPipe;
exports.CORE_PIPES = [
    order_by_pipe_1.OrderByPipe,
    search_pipe_1.SearchPipe,
    filter_pipe_1.FilterPipe
];
//# sourceMappingURL=index.js.map