export declare class FilterPipe {
    private filterByString(filter);
    private filterByBoolean(filter);
    private filterByObject(filter);
    private getValue(value);
    private filterDefault(filter);
    private isNumber(value);
    private isObject(value);
    transform(array: any[], filter: any): any;
}
