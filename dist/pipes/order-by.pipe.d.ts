import { PipeTransform } from '@angular/core';
export declare class OrderByPipe implements PipeTransform {
    private static _orderByComparator(a, b);
    transform(input: any[], [config]: any[]): any[];
}
