import { OrderByPipe } from './order-by.pipe';
import { SearchPipe } from './search.pipe';
import { FilterPipe } from './filter.pipe';
export { OrderByPipe, SearchPipe, FilterPipe };
export declare const CORE_PIPES: (typeof SearchPipe | typeof FilterPipe)[];
