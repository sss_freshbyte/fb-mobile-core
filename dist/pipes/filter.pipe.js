"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var FilterPipe = (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.filterByString = function (filter) {
        if (filter) {
            filter = filter.toLowerCase();
        }
        return function (value) {
            var result = !filter || (value ? value.toLowerCase().indexOf(filter) !== -1 : false);
            return result;
        };
    };
    FilterPipe.prototype.filterByBoolean = function (filter) {
        return function (value) {
            return Boolean(value) === filter;
        };
    };
    FilterPipe.prototype.filterByObject = function (filter) {
        var _this = this;
        return function (value) {
            var isMatching = false;
            for (var key in filter) {
                if (!value.hasOwnProperty(key) && !Object.getOwnPropertyDescriptor(Object.getPrototypeOf(value), key)) {
                    return false;
                }
                var val = _this.getValue(value[key]);
                var type = typeof filter[key];
                if (type === 'boolean') {
                    if (_this.filterByBoolean(filter[key])(val)) {
                        isMatching = true;
                    }
                }
                else if (type === 'string') {
                    if (_this.filterByString(filter[key])(val) === true) {
                        isMatching = true;
                    }
                }
                else if (type === 'object') {
                    if (_this.filterByObject(filter[key])(val)) {
                        isMatching = true;
                    }
                }
                else {
                    if (_this.filterDefault(filter[key])(val) === true) {
                        isMatching = true;
                    }
                }
            }
            return isMatching;
        };
    };
    FilterPipe.prototype.getValue = function (value) {
        return typeof value === 'function' ? value() : value;
    };
    FilterPipe.prototype.filterDefault = function (filter) {
        return function (value) {
            return !filter || filter == value;
        };
    };
    FilterPipe.prototype.isNumber = function (value) {
        return !isNaN(parseInt(value, 10)) && isFinite(value);
    };
    FilterPipe.prototype.isObject = function (value) {
        return typeof value === 'object';
    };
    FilterPipe.prototype.transform = function (array, filter) {
        var type = typeof filter;
        if (!array || filter === undefined || filter === '' || filter.length < 1) {
            return array;
        }
        if (type === 'boolean') {
            return array.filter(this.filterByBoolean(filter));
        }
        if (type === 'string') {
            if (this.isObject(array[0])) {
                return array.filter(function (item) {
                    var match = false;
                    for (var p in item) {
                        if (!item.hasOwnProperty(p)) {
                            return;
                        }
                        if (item[p].indexOf(filter) > -1) {
                            match = true;
                        }
                    }
                    return match;
                });
            }
            if (this.isNumber(filter)) {
                return array.filter(this.filterDefault(filter));
            }
            return array.filter(this.filterByString(filter));
        }
        if (type === 'object') {
            return array.filter(this.filterByObject(filter));
        }
        if (type === 'function') {
            return array.filter(filter);
        }
        return array.filter(this.filterDefault(filter));
    };
    return FilterPipe;
}());
FilterPipe.decorators = [
    { type: core_1.Pipe, args: [{
                name: 'filterBy',
                pure: false
            },] },
    { type: core_1.Injectable },
];
FilterPipe.ctorParameters = function () { return []; };
exports.FilterPipe = FilterPipe;
//# sourceMappingURL=filter.pipe.js.map