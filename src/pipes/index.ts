import { OrderByPipe }  from './order-by.pipe';
import { SearchPipe }   from './search.pipe';
import { FilterPipe }   from './filter.pipe';

export {
	OrderByPipe,
	SearchPipe,
	FilterPipe,
}

export const CORE_PIPES = [
	OrderByPipe,
	SearchPipe,
	FilterPipe
];