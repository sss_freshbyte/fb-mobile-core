import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Injectable()
@Pipe({ name: 'search', pure: false })
export class SearchPipe implements PipeTransform {

	public transform(data: any[], [fields, term]: [string[], string] ): any[] {

		let parts: string[] = term && term.trim().toLowerCase().split(/\s+/);
		if (!parts || !parts.length) return data;

		return data.filter(item => {
			return parts.every(part => {
				return fields.some(field => {
					return item[field].toLowerCase().indexOf(part) > -1;
				});
			});
		});

	}

}