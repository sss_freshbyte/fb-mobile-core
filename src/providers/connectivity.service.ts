import { Injectable }       from '@angular/core';
import { Network }          from '@ionic-native/network';
import { Events }           from 'ionic-angular';
import { PublishEvents }    from '../models/publish-events';


declare let navigator: any;

@Injectable()
export class ConnectivityService {

	private _window: any;
	private disconnectSubscription: any;
	private connectSubscription: any;
	private eventPublishEnabled: boolean = true;

	constructor(private _events: Events, private _network: Network) {
		this._window = window;

		if (this._window.cordova) {
			if (navigator && navigator.connection ) {
				this._isConnected = navigator.connection.type !== 'none';
			}
		} else {
			this._isConnected = true;
		}

		this.disconnectSubscription = this._network.onDisconnect().subscribe(() => {
			this._isConnected = false;
			if (this.eventPublishEnabled) {
				this._events.publish(PublishEvents.Offline);
			}
		});

		this.connectSubscription = this._network.onConnect().subscribe(() => {
			this._isConnected = true;
			if (this.eventPublishEnabled) {
				setTimeout(() => {
					this._events.publish(PublishEvents.Online);
				}, 2000);
			}
		});
	}

	public enableConnectionMonitoring(): void {
		this.eventPublishEnabled = true;
	}

	public disableConnectionMonitoring(): void {
		this.eventPublishEnabled = false;
	}

	private _isConnected: boolean;
	public get isConnected(): boolean {
		return this._isConnected;
	}
}