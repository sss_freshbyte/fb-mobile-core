import { Injectable } from '@angular/core';
import { IDbTable }   from '../models';

@Injectable()
export class DbConfig {

	private _tables: Array<IDbTable>;
	public get tables(): Array<IDbTable> {
		return this._tables;
	}

	// private _tables: Array<IDbTable> = [
	// 	{
	// 		name: 'priceSettings',
	// 		isObject: true,
	// 		columns: [
	// 			{ name: 'round', type: 'TEXT' },
	// 			{ name: 'roundTo', type: 'TEXT' },
	// 			{ name: 'lastPriceOverride', type: 'NUMBER' },
	// 			{ name: 'substituteForMissingActual', type: 'TEXT' },
	// 			{ name: 'currentMarketDate', type: 'TEXT' },
	// 			{ name: 'futureMarketDate', type: 'TEXT' },
	// 			{ name: 'calculatePriceWhenNoPriceLevel', type: 'NUMBER' },
	// 			{ name: 'calculateBreakdownBeforeFactor', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'priceLevels',
	// 		columns: [
	// 			{ name: 'priceGroupId', type: 'TEXT' },
	// 			{ name: 'priceLevelMatch', type: 'TEXT' },
	// 			{ name: 'breakdownPercent', type: 'NUMBER' },
	// 			{ name: 'breakdownDollar', type: 'NUMBER' },
	// 			{ name: 'markupPercent', type: 'NUMBER' },
	// 			{ name: 'markupDollar', type: 'NUMBER' },
	// 			{ name: 'costBasis', type: 'TEXT' }
	// 		]
	// 	},
	// 	{
	// 		name: 'dateOverrides',
	// 		columns: [
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'uom', type: 'TEXT' },
	// 			{ name: 'pricingUom', type: 'TEXT' },
	// 			{ name: 'fixedOverride', type: 'NUMBER' },
	// 			{ name: 'markupPercent', type: 'NUMBER' },
	// 			{ name: 'markupDollar', type: 'NUMBER' },
	// 			{ name: 'fromDate', type: 'TEXT' },
	// 			{ name: 'toDate', type: 'TEXT' },
	// 			{ name: 'useForAllUom', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'customerOverrides',
	// 		columns: [
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'uom', type: 'TEXT' },
	// 			{ name: 'pricingUom', type: 'TEXT' },
	// 			{ name: 'fixedOverride', type: 'NUMBER' },
	// 			{ name: 'markupPercent', type: 'NUMBER' },
	// 			{ name: 'markupDollar', type: 'NUMBER' },
	// 			{ name: 'fromDate', type: 'TEXT' },
	// 			{ name: 'toDate', type: 'TEXT' },
	// 			{ name: 'useForAllUom', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'priceLevelOverrides',
	// 		columns: [
	// 			{ name: 'priceGroupId', type: 'TEXT' },
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'uom', type: 'TEXT' },
	// 			{ name: 'pricingUom', type: 'TEXT' },
	// 			{ name: 'fixedOverride', type: 'NUMBER' },
	// 			{ name: 'markupPercent', type: 'NUMBER' },
	// 			{ name: 'markupDollar', type: 'NUMBER' },
	// 			{ name: 'fromDate', type: 'TEXT' },
	// 			{ name: 'toDate', type: 'TEXT' },
	// 			{ name: 'useForAllUom', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'items',
	// 		columns: [
	// 			{ name: 'itemCode', type: 'TEXT PRIMARY KEY' },
	// 			{ name: 'description', type: 'TEXT' },
	// 			{ name: 'lastCost', type: 'real' },
	// 			{ name: 'currentMarketPrice', type: 'real' },
	// 			{ name: 'futureMarketPrice', type: 'real' },
	// 			{ name: 'itemType', type: 'TEXT' },
	// 			{ name: 'excludeCustomerFromLastPriceOverride', type: 'NUMBER' },
	// 			{ name: 'priceGroupMatch', type: 'TEXT' },
	// 			{ name: 'discountGroupMatch', type: 'TEXT' },
	// 			{ name: 'itemUomKey', type: 'NUMBER' },
	// 			{ name: 'upcCode', type: 'TEXT' }
	// 		]
	// 	},
	// 	{
	// 		name: 'itemUoms',
	// 		columns: [
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'uom', type: 'TEXT' },
	// 			{ name: 'conversionFactor', type: 'NUMBER' },
	// 			{ name: 'breakdownPercent', type: 'NUMBER' },
	// 			{ name: 'breakdownDollar', type: 'NUMBER' },
	// 			{ name: 'isWeightUom', type: 'NUMBER' },
	// 			{ name: 'isMaster', type: 'INTEGER' },
	// 			{ name: 'preventPriceLevelBreakdown', type: 'NUMBER' },
	// 			{ name: 'isNetWeightPricingUom', type: 'INTEGER' },
	// 			{ name: 'isCatchWeightOrderUom', type: 'INTEGER' },
	// 			{ name: 'isPreferredUom', type: 'TEXT' }
	// 		]
	// 	},
	// 	{
	// 		name: 'customerItemSpecifications',
	// 		columns: [
	// 			{ name: 'id', type: 'TEXT' },
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'uomId', type: 'TEXT' },
	// 			{ name: 'lastCostForOverride', type: 'real' },
	// 			{ name: 'pricedUom', type: 'TEXT' },
	// 			{ name: 'standardOrder', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'priceGroups',
	// 		columns: [
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'priceGroup', type: 'TEXT' },
	// 			{ name: 'discountGroup', type: 'TEXT' }
	// 		]
	// 	},
	// 	{
	// 		name: 'customers',
	// 		columns: [
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'name', type: 'TEXT' },
	// 			{ name: 'salesId', type: 'TEXT' },
	// 			{ name: 'phone', type: 'TEXT' },
	// 			{ name: 'fax', type: 'TEXT' },
	// 			{ name: 'email', type: 'TEXT' },
	// 			{ name: 'termDays', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'discounts',
	// 		columns: [
	// 			{ name: 'discountGroupId', type: 'TEXT' },
	// 			{ name: 'discountMatch', type: 'TEXT' },
	// 			{ name: 'discountPercent', type: 'NUMBER' },
	// 			{ name: 'discountAmount', type: 'NUMBER' },
	// 		]
	// 	},
	// 	{
	// 		name: 'salesOrders',
	// 		columns: [
	// 			{ name: 'id', type: 'INTEGER PRIMARY KEY AUTOINCREMENT' },
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'customerName', type: 'TEXT' },
	// 			{ name: 'customerTermDays', type: 'NUMBER' },
	// 			{ name: 'status', type: 'TEXT' },
	// 			{ name: 'poNumber', type: 'TEXT' },
	// 			{ name: 'deliveryMethod', type: 'TEXT' },
	// 			{ name: 'deliveryDate', type: 'TEXT' },
	// 			{ name: 'orderDate', type: 'NUMBER' },
	// 			{ name: 'shipToName', type: 'TEXT' },
	// 			{ name: 'shipToAddressNumber', type: 'INTEGER' },
	// 			{ name: 'shipToAddress1', type: 'TEXT' },
	// 			{ name: 'shipToAddress2', type: 'TEXT' },
	// 			{ name: 'shipToCity', type: 'TEXT' },
	// 			{ name: 'shipToState', type: 'TEXT' },
	// 			{ name: 'shipToZip', type: 'TEXT' },
	// 			{ name: 'shipToCountry', type: 'TEXT' },
	// 			{ name: 'shipToPhone', type: 'TEXT' },
	// 			{ name: 'shipToFax', type: 'TEXT' },
	// 			{ name: 'shipToEmail', type: 'TEXT' },
	// 			{ name: 'notes', type: 'TEXT' },
	// 			{ name: 'lastError', type: 'TEXT' }
	// 		]
	// 	},
	// 	{
	// 		name: 'lineItems',
	// 		columns: [
	// 			{ name: 'id', type: 'INTEGER PRIMARY KEY AUTOINCREMENT' },
	// 			{ name: 'salesOrderId', type: 'NUMBER' },
	// 			{ name: 'itemCode', type: 'TEXT' },
	// 			{ name: 'itemType', type: 'TEXT' },
	// 			{ name: 'description', type: 'TEXT' },
	// 			{ name: 'quantity', type: 'NUMBER' },
	// 			{ name: 'uom', type: 'TEXT' },
	// 			{ name: 'pricingUom', type: 'TEXT' },
	// 			{ name: 'unitPriceWithDiscount', type: 'NUMBER' },
	// 			{ name: 'extendedPriceWithDiscount', type: 'NUMBER' },
	// 			{ name: 'extendedDiscount', type: 'NUMBER' },
	// 			{ name: 'isPriceOverridden', type: 'NUMBER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'shipToAddresses',
	// 		columns: [
	// 			{ name: 'number', type: 'INTEGER' },
	// 			{ name: 'customerId', type: 'TEXT' },
	// 			{ name: 'customerLocationId', type: 'TEXT' },
	// 			{ name: 'name', type: 'TEXT' },
	// 			{ name: 'contactName', type: 'TEXT' },
	// 			{ name: 'contactTitle', type: 'TEXT' },
	// 			{ name: 'addressLineOne', type: 'TEXT' },
	// 			{ name: 'addressLineTwo', type: 'TEXT' },
	// 			{ name: 'city', type: 'TEXT' },
	// 			{ name: 'state', type: 'TEXT' },
	// 			{ name: 'zip', type: 'TEXT' },
	// 			{ name: 'country', type: 'TEXT' },
	// 			{ name: 'phone', type: 'TEXT' },
	// 			{ name: 'fax', type: 'TEXT' },
	// 			{ name: 'email', type: 'TEXT' },
	// 			{ name: 'isDefault', type: 'INTEGER' }
	// 		]
	// 	},
	// 	{
	// 		name: 'pickSettings',
	// 		isObject: true,
	// 		columns: [
	// 			{ name: 'delimiter', type: 'TEXT' },
	// 			{ name: 'identifier', type: 'TEXT' }
	// 		]
	// 	}
	// ];
}