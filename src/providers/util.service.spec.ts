import { UtilService } from './util.service';
import { DateService } from './date.service';

let _classUnderTest: UtilService;
let _dateSrvc: DateService;
let _date: Date;

describe('UtilService', () => {

	beforeEach(() => {
		_date = new Date();
		_dateSrvc = jasmine.createSpyObj('DateService', ['now']);
		(<jasmine.Spy>(_dateSrvc.now)).and.returnValue(_date);
		_classUnderTest = new UtilService(_dateSrvc);
	});

	describe('date', () => {
		it('should return date string without time', () => {
			let date: string = _classUnderTest.date();
			let result = date.match(/^\d{4}-\d{2}-\d{2}$/);

			expect(result).toBeDefined()
		});
	});

	describe('year', () => {
		it('should return current year', () => {
			let now: number = _date.getFullYear();
			let max: number = _classUnderTest.year();

			expect(max).toEqual(now);
		});

		it('should add years', () => {
			let additionalYears: number = 5;
		 	let now: number = _date.getFullYear();
			let max: number = _classUnderTest.year(additionalYears);

			expect(max).toEqual(now + additionalYears);
		});

	});

	describe('toBool', () => {

		it('should return true for boolean', () => {
			expect(UtilService.toBool(true)).toBe(true);
		});

		it('should return true for \'true\'', () => {
			expect(UtilService.toBool('true')).toBe(true);
		});

		it('should be case insensitive for strings', () => {
			expect(UtilService.toBool('TrUe')).toBe(true);
		});

		it('should return true for 1', () => {
			expect(UtilService.toBool(1)).toBe(true);
		});

		it('should return true for \'1\'', () => {
			expect(UtilService.toBool('1')).toBe(true);
		});
	});
});