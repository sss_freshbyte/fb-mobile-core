import { Injectable }       from '@angular/core';

import { LoggerService }    from './logger.service';
import { LocalDb }          from './local-db.service';

interface Cordova {
	getAppVersion: any;
}

@Injectable()
export class AppSettingsService {

	private _window: any;

	constructor(private _logger: LoggerService, private _db: LocalDb) {
		this._window = window;
		this.load();
	}

	public get ApiUrl(): string {
		return this.buildUrl(this._savedApiUrl);
	}

	private _authToken: string;
	public get authToken(): string {
		return this._authToken;
	}

	public set authToken(value: string) {
		this._authToken = value;
	}

	private _savedApiUrl: string;

	public get SavedApiUrl(): string {
		return this._savedApiUrl;
	}

	private _binaryVersion: string;

	public get BinaryVersion(): string {
		return this._binaryVersion;
	}

	public load(): Promise<any> {
		return this.loadApiUrl()
			.then(() => {
				if (this._window.cordova) {
					let cordova = <Cordova>(this._window.cordova);
					return cordova.getAppVersion.getVersionNumber();
				} else {
					return Promise.resolve('Browser');
				}
			})
			.then(version => {
				this._binaryVersion = version;
				return this.getAuthToken();
			})
			.then(token => this._authToken = token)
			.catch(err => this._logger.error(err));
	}

	public loadApiUrl(): Promise<any> {
		return this._db.get('apiUrl')
			.then(url => {
				this._savedApiUrl = url;
			});
	}

	public setApiUrl(url: string): Promise<any> {
		return this._db.set('apiUrl', url)
			.then(() => {
				this._savedApiUrl = url;
			});
	}

	public setSession(session: any): Promise<any> {
		this.authToken = session.token;
		return this._db.set('session', session);
	}

	public getSession(): Promise<any> {
		return this._db.get('session');
	}

	public resetSession(): Promise<any> {
		return this.setSession({});
	}

	public buildUrl(url: string): string {
		if (url.indexOf('http') === -1) {
			let result = 'https://' + url + '.edible-online.com/api';
			return result;
		}
		return url;
	}

	public getUserName(): Promise<string> {
		return this._db.get('session')
			.then((session) => {
				return session.edibleUserName;
			});
	}

	public getAuthToken(): Promise<string> {
		return this._db.get('session')
			.then((session) => {
				let token = session ? session.token || '' : '';
				return Promise.resolve(token);
			});
	}
}
