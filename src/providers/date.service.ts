import {Injectable} from '@angular/core';

@Injectable()
export class DateService {

	/*
	* @deprecated
	* */
	public static dateWithoutTime(): string {
		let d = new Date(),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}

	/*
	 * @deprecated
	 * */
	public static maxDateForPicker(): number {
		let d = new Date(),
			nextYear = d.getFullYear() + 1;

		return nextYear;
	}

	public now(): Date {
		return new Date();
	}
}
