import { Injectable } from '@angular/core';
import { DateService } from './date.service';

@Injectable()
export class UtilService {

	constructor(private _dateSrvc: DateService) {}

	public date(addDays?: number): string {
		let targetDate: Date = this._dateSrvc.now();
		targetDate.setDate(targetDate.getDate() + (addDays || 0));
		let year: string = targetDate.getFullYear().toString();
		let mth: string = ('00' + (targetDate.getMonth() + 1)).slice(-2);
		let day: string = ('00' + targetDate.getDate()).slice(-2);
		return `${year}-${mth}-${day}`;
	}

	public static now(addDays?: number): string {
		let targetDate: Date = new Date();
		targetDate.setDate(targetDate.getDate() + (addDays || 0));
		let year: string = targetDate.getFullYear().toString();
		let mth: string = ('00' + (targetDate.getMonth() + 1)).slice(-2);
		let day: string = ('00' + targetDate.getDate()).slice(-2);
		return `${year}-${mth}-${day}`;
	}

	public year(addYears?: number): number {
		let d = this._dateSrvc.now(),
			yr = d.getFullYear() + (addYears || 0);

		return yr;
	}

	public static toBool(value: any): boolean {
		if (typeof value === 'string') {
			value = value.toLowerCase();
		} else if (typeof value === 'boolean') {
			return value;
		}
		switch (value) {
			case 'true':
				return true;
			case 1:
				return true;
			case '1':
				return true;
			default:
				return false;
		}
	}
}