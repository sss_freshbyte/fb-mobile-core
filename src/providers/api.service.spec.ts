import { Http, RequestOptions } from '@angular/http';
import { Events } from 'ionic-angular';
import { Api } from './api.service';
import { AppSettingsService } from './app-settings.service';
import { PublishEvents } from '../models/publish-events';
import { Observable } from 'rxjs';

describe('Api', () => {

	let _classUnderTest: Api;
	let _appSettingsSrvc: AppSettingsService;
	let _events: Events;
	let _http: Http;

	let _mockResponse: any;
	let _apiUrl: string = 'foo';
	let _requestOptions: RequestOptions;
	let _params: Object;

	beforeEach(() => {
		_mockResponse = {};
		_http = jasmine.createSpyObj('fbHttp', ['get', 'post', 'put', 'patch', 'delete']);
		_requestOptions = jasmine.createSpyObj('requestOptions', ['_']);
		_appSettingsSrvc = jasmine.createSpyObj('appSettings', ['get']);
		_appSettingsSrvc['ApiUrl'] = _apiUrl;
		_events = jasmine.createSpyObj('events', ['publish']);
		_classUnderTest = new Api(_http, _appSettingsSrvc, _events);
	});

	describe('get', () => {

		beforeEach(() => {
			_params = {
				propOne: 'yeah'
			};
			(<jasmine.Spy>(_http.get)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should publish loading event', (done) => {
			_classUnderTest.get('', _params, _requestOptions).then(() => {
				expect(_events.publish).toHaveBeenCalledWith(PublishEvents.DismissLoading);
				done();
			});
		});

		it('should call get on http', (done) => {
			_classUnderTest.get('').then(() => {
				expect(_http.get).toHaveBeenCalledWith(_apiUrl + '//', new RequestOptions());
				done();
			});
		});

		it('should return Promise with Response', (done) => {
			_classUnderTest.get('').then((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('get$', () => {

		beforeEach(() => {
			(<jasmine.Spy>(_http.get)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should call get on http', (done) => {
			_classUnderTest.get$('').subscribe(() => {
				expect(_http.get).toHaveBeenCalledWith(_apiUrl + '//', new RequestOptions());
				done();
			});
		});

		it('should return Observable with Response', (done) => {
			_classUnderTest.get$('').subscribe((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('post', () => {

		let _postBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.post)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should publish loading event', (done) => {
			_classUnderTest.post('', _postBody).then(() => {
				expect(_events.publish).toHaveBeenCalledWith(PublishEvents.DismissLoading);
				done();
			});
		});

		it('should call post on http', (done) => {
			_classUnderTest.post('', _postBody).then(() => {
				expect(_http.post).toHaveBeenCalledWith(_apiUrl + '//', _postBody, undefined);
				done();
			});
		});

		it('should return Promise with Response', (done) => {
			_classUnderTest.post('', _postBody).then((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('post$', () => {

		let _postBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.post)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should call post$ on http', (done) => {
			_classUnderTest.post$('', _postBody).subscribe(() => {
				expect(_http.post).toHaveBeenCalledWith(_apiUrl + '//', _postBody, undefined);
				done();
			});
		});

		it('should return Observable with Response', (done) => {
			_classUnderTest.post$('', _postBody).subscribe((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('put', () => {

		let _putBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.put)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should publish loading event', (done) => {
			_classUnderTest.put('', _putBody).then(() => {
				expect(_events.publish).toHaveBeenCalledWith(PublishEvents.DismissLoading);
				done();
			});
		});

		it('should call put on http', (done) => {
			_classUnderTest.put('', _putBody).then(() => {
				expect(_http.put).toHaveBeenCalledWith(_apiUrl + '//', _putBody, undefined);
				done();
			});
		});

		it('should return Promise with Response', (done) => {
			_classUnderTest.put('', _putBody).then((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('put$', () => {

		let _putBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.put)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should call put$ on http', (done) => {
			_classUnderTest.put$('', _putBody).subscribe(() => {
				expect(_http.put).toHaveBeenCalledWith(_apiUrl + '//', _putBody, undefined);
				done();
			});
		});

		it('should return Observable with Response', (done) => {
			_classUnderTest.put$('', _putBody).subscribe((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('patch', () => {

		let _patchBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.patch)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should publish loading event', (done) => {
			_classUnderTest.patch('', _patchBody).then(() => {
				expect(_events.publish).toHaveBeenCalledWith(PublishEvents.DismissLoading);
				done();
			});
		});

		it('should call patch on http', (done) => {
			_classUnderTest.patch('', _patchBody).then(() => {
				expect(_http.patch).toHaveBeenCalledWith(_apiUrl + '//', _patchBody, undefined);
				done();
			});
		});

		it('should return Promise with Response', (done) => {
			_classUnderTest.patch('', _patchBody).then((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('patch$', () => {

		let _patchBody = {};

		beforeEach(() => {
			(<jasmine.Spy>(_http.patch)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should call patch$ on http', (done) => {
			_classUnderTest.patch$('', _patchBody).subscribe(() => {
				expect(_http.patch).toHaveBeenCalledWith(_apiUrl + '//', _patchBody, undefined);
				done();
			});
		});

		it('should return Observable with Response', (done) => {
			_classUnderTest.patch$('', _patchBody).subscribe((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('delete', () => {

		beforeEach(() => {
			(<jasmine.Spy>(_http.delete)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should publish loading event', (done) => {
			_classUnderTest.delete('').then(() => {
				expect(_events.publish).toHaveBeenCalledWith(PublishEvents.DismissLoading);
				done();
			});
		});

		it('should call delete on http', (done) => {
			_classUnderTest.delete('').then(() => {
				expect(_http.delete).toHaveBeenCalledWith(_apiUrl + '//', undefined);
				done();
			});
		});

		it('should return Promise with Response', (done) => {
			_classUnderTest.delete('').then((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});

	describe('delete$', () => {

		beforeEach(() => {
			(<jasmine.Spy>(_http.delete)).and.returnValue(Observable.of(_mockResponse));
		});

		it('should call delete on http', (done) => {
			_classUnderTest.delete$('').subscribe(() => {
				expect(_http.delete).toHaveBeenCalledWith(_apiUrl + '//', undefined);
				done();
			});
		});

		it('should return Observable with Response', (done) => {
			_classUnderTest.delete$('').subscribe((actual) => {
				expect(actual).toEqual(_mockResponse);
				done();
			});
		});
	});
});