import {Injectable} from '@angular/core';
import {Http, RequestOptions, URLSearchParams} from '@angular/http';
import { AlertController, Events } from 'ionic-angular';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {AppSettingsService} from './app-settings.service';
import {PublishEvents} from '../models/publish-events';
import { LoggerService } from './logger.service';

@Injectable()
export class Api {

	constructor(private _http: Http,
				private _appSettingsSrvc: AppSettingsService,
				private _events: Events,
				private _logger: LoggerService) {
	}

	protected resourceUrl(urlSegment?: string): string {
		return this._appSettingsSrvc.ApiUrl + '/' + urlSegment + '/';
	}

	public buildParams(params?: any, options?: RequestOptions): void {
		if (params) {
			let p = new URLSearchParams();
			for (let k in params) {
				p.set(k, params[k]);
			}
			options.search = !options.search && p || options.search;
		}
	}

	public get$(endpoint: string, params?: any, options?: RequestOptions): Observable<any> {
		this.notifyLoad();
		if (!options) {
			options = new RequestOptions();
		}
		this.buildParams(params, options);

		return this._http.get(this.resourceUrl(endpoint), options)
			.map(result => this.notifyDismiss(result))
			.catch(err => this.handleError(err));
	}

	public get(endpoint: string, params?: any, options?: RequestOptions): Promise<any> {
		this.notifyLoad();
		return this.get$(endpoint, params, options)
			.toPromise()
			.then(result => this.notifyDismiss(result));
	}

	public post$(endpoint: string, body: any, options?: RequestOptions): Observable<any> {
		this.notifyLoad();
		return this._http.post(this.resourceUrl(endpoint), body, options)
			.map(result => this.notifyDismiss(result))
			.catch(err => this.handleError(err));
	}

	public post(endpoint: string, body: any, options?: RequestOptions): Promise<any> {
		this.notifyLoad();
		return this.post$(endpoint, body, options)
			.map(result => this.notifyDismiss(result))
			.toPromise();
	}

	public put$(endpoint: string, body: any, options?: RequestOptions): Observable<any> {
		this.notifyLoad();
		return this._http.put(this.resourceUrl(endpoint), body, options)
			.map(result => this.notifyDismiss(result))
			.catch(err => this.handleError(err));

	}

	public put(endpoint: string, body: any, options?: RequestOptions): Promise<any> {
		this.notifyLoad();
		return this.put$(endpoint, body, options)
			.map(result => this.notifyDismiss(result))
			.toPromise();
	}

	public delete$(endpoint: string, options?: RequestOptions): Observable<any> {
		this.notifyLoad();
		return this._http.delete(this.resourceUrl(endpoint), options)
			.map(result => this.notifyDismiss(result))
			.catch(err => this.handleError(err));
	}

	public delete(endpoint: string, options?: RequestOptions): Promise<any> {
		this.notifyLoad();
		return this.delete$(endpoint, options)
			.map(result => this.notifyDismiss(result))
			.toPromise();
	}

	public patch$(endpoint: string, body: any, options?: RequestOptions): Observable<any> {
		this.notifyLoad();
		return this._http.patch(this.resourceUrl(endpoint), body, options)
			.map(result => this.notifyDismiss(result))
			.catch(err => this.handleError(err));
	}

	public patch(endpoint: string, body: any, options?: RequestOptions): Promise<any> {
		this.notifyLoad();
		return this.patch$(endpoint, body, options)
			.map(result => this.notifyDismiss(result))
			.toPromise();
	}

	private notifyLoad(): void {
		this._events.publish(PublishEvents.DisplayLoading);
	}

	private notifyDismiss(result: any): any {
		this._events.publish(PublishEvents.DismissLoading);
		return result;
	}

	protected handleError(error: any): any{
		let errorMessage: string = '';
		if (error.message) {
			errorMessage = error.message;
		}
		else if (error._body) {
			try {
				errorMessage = JSON.parse(error._body).message;
			} catch (e) {
				errorMessage = '';
			}
		}
		let msg = (errorMessage) ? errorMessage :
			error.status ? error.status + ' - ' + error.statusText : 'Server error';
		this._logger.error(msg);
		this._events.publish(PublishEvents.Error, msg, error.status);
		return Observable.throw(msg);
	}
}