import {Injectable}                                   from '@angular/core';
import {Http, Headers, RequestOptionsArgs}            from '@angular/http';
import {Response, RequestOptions, ConnectionBackend}  from '@angular/http';
import {Events}                                       from 'ionic-angular';
import {Observable}                                   from 'rxjs/Rx';

import {AppSettingsService}                           from './app-settings.service';
import {LoggerService}                                from './logger.service';

@Injectable()
export class FbHttp extends Http {

	constructor(protected _backend: ConnectionBackend,
				protected _defaultOptions: RequestOptions,
				protected _appSettingsService: AppSettingsService,
				protected _events: Events,
				protected _logger: LoggerService) {
		super(_backend, _defaultOptions);
	}

	public buildHeaders(): Headers {
		let headers = new Headers();
		headers.append('AppVersion', this._appSettingsService.BinaryVersion);
		headers.append('Authorization', this._appSettingsService.authToken);
		headers.append('Content-Type', 'application/json');
		return headers;
	}

	public fbRequestOptions(originalOptions: RequestOptionsArgs): RequestOptions {
		let fbOptions = new RequestOptions({ headers: this.buildHeaders() });
		return fbOptions.merge(originalOptions);
	}

	public request(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.request(url, this.fbRequestOptions(options));
	}

	public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.get(url, this.fbRequestOptions(options));
	}

	public put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
		return super.put(url, body, this.fbRequestOptions(options));
	}

	public post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
		return super.post(url, body, this.fbRequestOptions(options));
	}

	public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.delete(url, this.fbRequestOptions(options));
	}

	public patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
		return super.patch(url, body, this.fbRequestOptions(options));
	}

	public head(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.request(url, this.fbRequestOptions(options));
	}
}