import { Injectable }      from '@angular/core';
import { Platform }        from 'ionic-angular';
import { LoggerService }   from './logger.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

declare let window: any;

@Injectable()
export class AnalyticsService {

	constructor(platform: Platform, logger: LoggerService, private _ga: GoogleAnalytics) {
		platform.ready().then(() => this._ga.startTrackerWithId('UA-84866094-2'))
			.then(() => logger.success('Google Analytics Initialized'))
			.catch(e => logger.log('Error starting Google Analytics', e));
	}

	public trackException(description: string, isFatal: boolean): Promise<void> {
		return this._ga.trackException(description, isFatal);
	}

	public trackView(screen: string, campaignUrl?: string, newSession?: boolean): Promise<void> {
		return this._ga.trackView(screen, campaignUrl, newSession || true);
	}

	public trackEvent(category: string, action: string, label?: string, value?: number, newSession?: boolean): Promise<void> {
		return this._ga.trackEvent(category, action, label, value, newSession || true);
	}

	public trackMetric(key: string, value?: string): Promise<void> {
		return this._ga.trackMetric(key, value);
	}

	public trackTiming(category: string, intervalMilliseconds: number, variable?: string, label?: string): Promise<void> {
		return this._ga.trackTiming(category, intervalMilliseconds, variable, label);
	}

	public addTransaction(id: string, affiliation: string, revenue: number, tax: number, shipping: number, currencyCode: string): Promise<void> {
		return this._ga.addTransaction(id, affiliation, revenue, tax, shipping, currencyCode);
	}

	public addTransactionItem(id: string, name: string, sku: string, category: string, price: number, quantity: number, currencyCode: string): Promise<void> {
		return this._ga.addTransactionItem(id, name, sku, category, price, quantity, currencyCode);
	}

	public addCustomerDimension(key: number, value?: string): Promise<void> {
		return this._ga.addCustomDimension(key, value);
	}

	public setUserId(value: string): Promise<void> {
		return this._ga.setUserId(value);
	}

	public setAppVersion(value: string): Promise<void> {
		return this._ga.setAppVersion(value);
	}

	public setAnonymousIP(value: boolean): Promise<void> {
		return this._ga.setAnonymizeIp(value);
	}

	public setOptOut(value: boolean): Promise<void> {
		return this._ga.setOptOut(value);
	}

	public setAllowIDFCollection(value: boolean): Promise<void> {
		return this._ga.setAllowIDFACollection(value);
	}

	public enableUncaughtExceptionReporting(enabled: boolean): Promise<void> {
		return this._ga.enableUncaughtExceptionReporting(enabled);
	}

	public debugMode(): Promise<void> {
		return this._ga.debugMode();
	}
}
