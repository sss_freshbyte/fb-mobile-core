import { AppSettingsService }     from './app-settings.service';
import { LoggerService }          from './logger.service';
import { LocalDb }                from './local-db.service';
import { TableSyncService }       from './table-sync.service';
import { UtilService }            from './util.service';
import { FbHttp }                 from './fb-http';
import { RdbStore }               from './rdb-store';
import { ConnectivityService }    from './connectivity.service';
import { DateService }            from './date.service';
import { DbConfig }               from './db-config.service';
import { QueryBuilderService }    from './query-builder.service';
import { Api }                    from './api.service';
import { AnalyticsService }       from './analytics.service';

export {
	Api,
	LoggerService,
	LocalDb,
	AppSettingsService,
	TableSyncService,
	UtilService,
	FbHttp,
	RdbStore,
	ConnectivityService,
	DateService,
	DbConfig,
	QueryBuilderService,
	AnalyticsService
};

export const CORE_PROVIDERS = [
	Api,
	LoggerService,
	LocalDb,
	AppSettingsService,
	FbHttp,
	TableSyncService,
	UtilService,
	RdbStore,
	ConnectivityService,
	DateService,
	DbConfig,
	QueryBuilderService,
	AnalyticsService
];