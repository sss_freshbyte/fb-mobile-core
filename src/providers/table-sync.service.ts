import { Injectable }       from '@angular/core';

import { Api }              from './api.service';
import { LocalDb }          from './local-db.service';

@Injectable()
export class TableSyncService {

	private _resource: string = 'sync';

	constructor(private _api: Api, private _db: LocalDb) {}

	public sync(tableName: string, startId: any): Promise<any> {
		let endpoint = tableName.includes('_') ? tableName.split('_')[1] : tableName;
		return this._db.dropAndCreate(tableName)
			.then(() => this._api.get(this._resource + '/' + endpoint, {startId: startId}))
			.then(r => this._db.seedTable(tableName, r.json()));
	}
}