import { NgModule }             from '@angular/core';
import { HttpModule }           from '@angular/http';
import { IonicApp, IonicModule} from 'ionic-angular';
import { GoogleAnalytics }      from '@ionic-native/google-analytics';
import { Keyboard }             from '@ionic-native/keyboard';
import { Network }              from '@ionic-native/network';
import { SQLite }               from '@ionic-native/sqlite';
import { TranslateModule }      from 'ng2-translate';

import { CORE_COMPONENTS }      from './components';
import { CORE_PIPES }           from './pipes';
import { CORE_PROVIDERS }       from './providers';
import { PopMenu }              from './components';

@NgModule({
	declarations: [CORE_COMPONENTS, CORE_PIPES, PopMenu],
	providers:[CORE_PROVIDERS, GoogleAnalytics, Keyboard, Network, SQLite],
	exports: [CORE_COMPONENTS, CORE_PIPES, PopMenu],
	imports: [HttpModule, TranslateModule, IonicModule],
	entryComponents: [IonicApp, PopMenu]
})
export class FbCoreModule {}