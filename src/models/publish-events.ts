export class PublishEvents {

	public static get Unauthorized(): string {
		return 'Unauthorized';
	}

	public static get Error(): string {
		return 'Error';
	}

	public static get Online(): string {
		return 'Online';
	}

	public static get Offline(): string {
		return 'Offline';
	}

	public static get UserLogin(): string {
		return 'UserLogin';
	}

	public static get UserLogout(): string {
		return 'UserLogout';
	}

	public static get FullSync(): string {
		return 'FullSyncRequest';
	}

	public static get FullSyncComplete(): string {
		return 'FullSyncComplete';
	}

	public static get DisplayLoading(): string {
		return 'DisplayLoading';
	}

	public static get DismissLoading(): string {
		return 'DismissLoading';
	}

	public static get DisableDefaultLoading(): string {
		return 'DisableDefaultLoading';
	}

	public static get EnableDefaultLoading(): string {
		return 'EnableDefaultLoading';
	}

	//Live Sales Orders
	public static get LiveSalesUpdateSalesOrder(): string {
		return 'LiveSalesUpdateSalesOrder';
	}

	public static get LiveSalesOrderUpdated(): string {
		return 'LiveSalesOrderUpdated';
	}

	public static get LiveSalesEditItemDetailsUpdated(): string {
		return 'LiveSalesEditItemDetailsUpdated';
	}

	public static get LiveSalesFinishedItemDetailEdit(): string {
		return 'LiveSalesFinishedItemDetailEdit';
	}
}