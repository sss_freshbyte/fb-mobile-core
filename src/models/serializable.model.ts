export class Serializable {

	public serialize(): string {
		return JSON.stringify(this.serializable);
	}

	public get serializable(): Object {
		let obj: any = {};
		for (let prop in this) {
			if (this.hasOwnProperty(prop) && prop.startsWith('_') && prop !== '_db' && prop !== '_logger' && prop !== '_pricingService') {
				if (this[prop] instanceof Array) {
					let arrayPropTemp: any[] = [];
					for (let arrayObj in this[prop]) {
						if (this[prop][arrayObj] instanceof Serializable) {
							let sObject:any =  this[prop][arrayObj];
							arrayPropTemp.push(sObject.serializable);
						}
					}
					obj[prop.slice(1)] = arrayPropTemp;
				} else {
					obj[prop.slice(1)] = this[prop];
				}
			}
		}
		return obj;
	}
}