/**
 * Taken from https://github.com/yuyang041060120/ng2-validation
 * Todo - use ng2-validators when fixed
 */
import {ValidatorFn, AbstractControl, Validators} from '@angular/forms';

export class CustomValidators {

	static min(min: number): ValidatorFn {
		return (control: AbstractControl): {[key: string]: any} => {
			if (isPresent(Validators.required(control))) return null;

			let v: number = control.value;
			return v >= min ? null : {'min': true};
		};
	}

	/**
	 * Validator that requires controls to have a value of a max value.
	 */
	static max(max: number): ValidatorFn {
		return (control: AbstractControl): {[key: string]: any} => {
			if (isPresent(Validators.required(control))) return null;

			let v: number = control.value;
			return v <= max ? null : {'max': true};
		};
	}
}

export function isPresent(obj: any): boolean {
	'use strict';
	return obj !== undefined && obj !== null;
}