export interface NavItem {
	title: string;
	page: {};
	isConnectionRequired: boolean;
}