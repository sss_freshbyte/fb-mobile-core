import { Serializable } from './serializable.model';

export class TestClass extends Serializable {

	private _propertyOne: string;
	public get propertyOne(): string {
		return this._propertyOne;
	}

	public set propertyOne(value: string) {
		this._propertyOne = value;
	}

	private _propertyTwo: number;
	public get propertyTwo(): number {
		return this._propertyTwo;
	}

	public set propertyTwo(value: number) {
		this._propertyTwo = value;
	}

	private _propertyThree: TestClass[];
	public get propertyThree(): TestClass[] {
		return this._propertyThree;
	}

	public set propertyThree(value: TestClass[]) {
		this._propertyThree = value;
	}

	private _db: any;
	public get db(): any {
		return this._db;
	}

	public set db(value: any) {
		this._db = value;
	}

	private _logger: any;
	public get logger(): any {
		return this._logger;
	}

	public set logger(value: any) {
		this._logger = value;
	}

	private _pricingService: any;
	public get pricingService(): any {
		return this._pricingService;
	}

	public set pricingService(value: any) {
		this._pricingService = value;
	}
}

describe('Serializable', () => {

	let classUnderTest: TestClass;

	beforeEach(() => {
		classUnderTest = new TestClass();
		classUnderTest.pricingService = {name: 'pricingService'};
		classUnderTest.db = {name: 'db'};
		classUnderTest.logger = {name: 'logger'};
		classUnderTest.propertyOne = 'PropertyOneValue';
		classUnderTest.propertyTwo = 42;
	});

	describe('serialize', () => {
		let serialized: any;

		beforeEach(() => {
			serialized = classUnderTest.serializable;
		});

		['db', 'pricingService', 'logger'].forEach((property) => {
			it('should serialize strip properties named ' + property, () => {
				expect(serialized[property]).toBeUndefined();
			});
		});

		it('should keep other properties', () => {
			expect(serialized.propertyOne).toEqual('PropertyOneValue');
			expect(serialized.propertyTwo).toEqual(42);
		});
	});

});