import { PopMenu }                  from './pop-menu.component';
import { FbNumericDirective }       from './fb-numeric.directive';
import { MaxValidator }             from './max.directive';
import { MinValidator }             from './min.directive';
import { KeyboardPop }              from './keyboard-pop.directive';

export {
	PopMenu,
	FbNumericDirective,
	MaxValidator,
	MinValidator,
	KeyboardPop
};

export const CORE_COMPONENTS = [
	FbNumericDirective,
	MaxValidator,
	MinValidator,
	KeyboardPop,
	PopMenu
];