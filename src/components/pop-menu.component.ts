import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';

@Component({
	template: `
	<ion-list>
		<ion-list-header>{{ title }}</ion-list-header>
		<button ion-item *ngFor="let b of buttons" (click)="fire(b)">
			<ion-icon name="{{ b.icon }}" *ngIf="b.cssClass === undefined"></ion-icon>
			<ion-icon name="{{ b.icon }}" *ngIf="b.cssClass === 'primary'" color="primary"></ion-icon>
			<ion-icon name="{{ b.icon }}" *ngIf="b.cssClass === 'secondary'" color="secondary"></ion-icon>
			<ion-icon name="{{ b.icon }}" *ngIf="b.cssClass === 'danger'" color="danger"></ion-icon>
			{{ b.text }}
		</button>
	</ion-list>`
})
export class PopMenu {

	constructor(
		private _viewController: ViewController,
		private _navParams: NavParams
	) {
		this.title = _navParams.get('title');
		this.buttons = _navParams.get('buttons');
	}

	public title: string;
	public buttons: Array<any>;

	public	fire(button: any): Promise<void> {
		return this._viewController.dismiss()
			.then(() => {
				button.handler();
			});
	}

}