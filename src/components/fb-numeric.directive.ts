import {Directive, Input} from '@angular/core';
import {NgModel} from '@angular/forms';

@Directive({
	selector: '[ngModel][fbNumeric]',
	providers: [NgModel],
	host: {
		'(ngModelChange)' : 'onInputChange($event)'
	}
})
export class FbNumericDirective {

	@Input() allowDecimal: boolean;

	constructor(
		private model: NgModel) {
	}

	public onInputChange(value?: any): any {
		let _value: number;
		if (value) {
			_value = this.allowDecimal ? value : Math.round(value);
		}
		this.model.valueAccessor.writeValue(_value);
	}
}