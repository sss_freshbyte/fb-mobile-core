/**
 * Taken from https://github.com/yuyang041060120/ng2-validation
 * Todo - use ng2-validators when fixed
 */
import { Directive, Input, forwardRef, OnInit }                     from '@angular/core';
import { NG_VALIDATORS, Validator, ValidatorFn, AbstractControl }   from '@angular/forms';
import { CustomValidators }                                         from '../models/custom-validators';

const MAX_VALIDATOR: any = {
	provide: NG_VALIDATORS,
	useExisting: forwardRef(() => MaxValidator),
	multi: true
};

@Directive({
	selector: '[max][formControlName],[max][formControl],[max][ngModel]',
	providers: [MAX_VALIDATOR]
})
export class MaxValidator implements Validator, OnInit {
	@Input() max: number;

	private validator: ValidatorFn;

	ngOnInit(): any {
		this.validator = CustomValidators.max(this.max);
	}

	validate(c: AbstractControl): {[key: string]: any} {
		return this.validator(c);
	}
}