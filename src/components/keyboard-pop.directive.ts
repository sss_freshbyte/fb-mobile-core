import { Component }    from '@angular/core';
import { Keyboard }     from '@ionic-native/keyboard';

@Component({
	selector: 'keyboard-pop',
	template: '<button class="keyboard-button"><ion-icon name="keypad" (click)="toggleKeyboard()"></ion-icon></button>',
})
export class KeyboardPop {
	constructor(private _keyboard: Keyboard) {
	}

	public toggleKeyboard(): void {
		throw new Error('No longer supported');
	}

	public close(): void{
		return this._keyboard.close();
	}

	public show(): void {
		return this._keyboard.show();
	}
}