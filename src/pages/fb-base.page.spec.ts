import { FbBasePage } from './fb-base.page';
import { Subscription } from 'rxjs/Subscription';

describe('BasePage', () => {

	let _classUnderTest: FbBasePage;

	class BlankPage extends FbBasePage {
	}

	beforeEach(() => {
		_classUnderTest = new BlankPage();
	});

	describe('ionViewDidUnload', () => {

		let subscription: Subscription = jasmine.createSpyObj('subscription', ['unsubscribe']);

		it('should call unsubscribe on all subscriptions', () => {
			_classUnderTest['subscriptions'].push(subscription);

			_classUnderTest.ionViewDidLeave();

			expect(subscription.unsubscribe).toHaveBeenCalled();

		});
	});
});