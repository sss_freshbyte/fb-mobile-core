import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export class FbBasePage implements OnDestroy {


	protected ngUnsubscribe: Subject<void> = new Subject<void>();
	protected subscriptions: Subscription[] = [];
	protected i18n: any = {};

	public ionViewDidLeave(): void {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	public highlight(e: any): void {
		e.target.select();
	}

	ngOnDestroy() {
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
	}
}