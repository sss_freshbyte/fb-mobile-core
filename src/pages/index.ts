import { FbBaseScrollPage } from './fb-base-scroll.page';
import { FbBasePage }       from './fb-base.page';
export {
	FbBasePage,
	FbBaseScrollPage
};
