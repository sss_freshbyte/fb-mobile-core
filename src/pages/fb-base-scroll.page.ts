import {Observable}     from 'rxjs/Observable';
import {FbBasePage}     from './fb-base.page';
import {LoggerService}  from '../providers/logger.service';

export class FbBaseScrollPage <T> extends FbBasePage {

	public itemList: T[];
	public searchQuery: string;
	public searchPredicate: Function;
	public preFilterPredicate: Function = (T?: Object) => {
		return true;
	};
	public _logger: LoggerService;

	constructor() {
		super();
		this.initList();
	}

	public initList(): void {
		this.itemList = [];
	}

	public loadMore(observable: Observable<T>, target: T[], infiniteScroll?: any, take: number = 30): void {
		this.subscriptions.push(
			observable.filter((item) => {
				let p: boolean = this.preFilterPredicate(item);
				let q: boolean = !this.searchQuery ? true : this.searchPredicate(item);
				return p && q;
			})
				.skip(target.length)
				.take(take)
				.subscribe((item) => {
					target.push(item);
				}, (err) => {
					this._logger.error(err);
				}, () => {
					if (infiniteScroll) {
						infiniteScroll.complete();
					}
				}));
	}
}